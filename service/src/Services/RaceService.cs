﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Service.Models.Database;

namespace Service.Services
{
    public class RaceService
    {
        private readonly IDynamoDBContext _dynamoDbContext;

        public RaceService(IDynamoDBContext dynamoDbContext)
        {
            _dynamoDbContext = dynamoDbContext;
        }

        public Task<Race> FetchRace(int raceId)
        {
            return _dynamoDbContext.LoadAsync<Race>(raceId);
        }

        public async Task<List<Race>> FetchRacesBetweenDates(DateTime after, DateTime before)
        {
            var search = _dynamoDbContext.ScanAsync<Race>
            (new[]
                {
                    new ScanCondition("Date", ScanOperator.Between, after, before)
                }
            );
            // Only the first page is returned, it isn't expected that more results than this will be needed.
            return await search.GetNextSetAsync();
        }

        public async Task PersistRace(Race race)
        {
            await _dynamoDbContext.SaveAsync(race);
        }
    }
}
