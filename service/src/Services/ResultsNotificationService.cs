﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Service.Models.Database;

namespace Service.Services
{
    public class ResultsNotificationService
    {
        private readonly IDynamoDBContext _dynamoDbContext;

        public ResultsNotificationService(IDynamoDBContext dynamoDbContext)
        {
            _dynamoDbContext = dynamoDbContext;
        }

        public async Task<ResultsNotification> FetchDueResultsNotification()
        {
            var response = _dynamoDbContext.ScanAsync<ResultsNotification>(new[]
                {
                    new ScanCondition("TimeThatResultsAreReleased", ScanOperator.LessThanOrEqual, DateTime.Now),
                    new ScanCondition("NotificationHasBeenSent", ScanOperator.Equal, false)
                }
            );
            return (await response.GetNextSetAsync()).FirstOrDefault();
        }

        public async Task SaveResultsNotification(ResultsNotification resultsNotification)
        {
            await _dynamoDbContext.SaveAsync(resultsNotification);
        }
        
        public async Task<ResultsNotification> UpsertResultsNotification
        (
            int raceId,
            long athleteId,
            Action<ResultsNotification> setProperties
        )
        {
            var resultsNotification = await FetchExistingResultsNotification(raceId, athleteId) ??
                                      new ResultsNotification {RaceId = raceId, AthleteId = athleteId};
            setProperties.Invoke(resultsNotification);
            await SaveResultsNotification(resultsNotification);
            return resultsNotification;
        }

        private async Task<ResultsNotification> FetchExistingResultsNotification(int raceId, long athleteId)
        {
            return await _dynamoDbContext.LoadAsync<ResultsNotification>(raceId, athleteId);
        }
    }
}
