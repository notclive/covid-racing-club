﻿using System;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Service.Models.Database;

namespace Service.Services
{
    public class AthleteService
    {
        private readonly IDynamoDBContext _dynamoDbContext;

        public AthleteService(IDynamoDBContext dynamoDbContext)
        {
            _dynamoDbContext = dynamoDbContext;
        }

        public async Task<Athlete> UpsertAthlete(long athleteId, Action<Athlete> setProperties)
        {
            var athlete = await FetchExistingAthlete(athleteId) ?? new Athlete {AthleteId = athleteId};
            setProperties.Invoke(athlete);
            await _dynamoDbContext.SaveAsync(athlete);
            return athlete;
        }

        private async Task<Athlete> FetchExistingAthlete(long athleteId)
        {
            return await _dynamoDbContext.LoadAsync<Athlete>(athleteId);
        }

        public async Task<Athlete> FetchAthlete(long athleteId)
        {
            return await _dynamoDbContext.LoadAsync<Athlete>(athleteId);
        }
    }
}
