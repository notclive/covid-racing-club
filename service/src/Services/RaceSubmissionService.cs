﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Service.Models.Database;

namespace Service.Services
{
    public class RaceSubmissionService
    {
        private readonly IDynamoDBContext _dynamoDbContext;

        public RaceSubmissionService(IDynamoDBContext dynamoDbContext)
        {
            _dynamoDbContext = dynamoDbContext;
        }

        public async Task<List<RaceSubmission>> FetchSubmissionsForRace(int raceId)
        {
            return await _dynamoDbContext.ScanAsync<RaceSubmission>
            (new List<ScanCondition>
                {
                    new ScanCondition("RaceId", ScanOperator.Equal, raceId)
                }
            ).GetRemainingAsync();
        }

        public async Task SaveSubmission(RaceSubmission raceSubmission)
        {
            await _dynamoDbContext.SaveAsync(raceSubmission);
        }
    }
}
