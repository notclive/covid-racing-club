﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Service.Models.Database;

namespace Service.Services
{
    public class RaceReminderService
    {
        private readonly IDynamoDBContext _dynamoDbContext;

        public RaceReminderService(IDynamoDBContext dynamoDbContext)
        {
            _dynamoDbContext = dynamoDbContext;
        }

        public async Task<RaceReminder> FetchDueRaceReminder()
        {
            var response = _dynamoDbContext.ScanAsync<RaceReminder>(new[]
                {
                    new ScanCondition("TimeThatRaceCanBeRun", ScanOperator.LessThanOrEqual, DateTime.Now),
                    new ScanCondition("ReminderHasBeenSent", ScanOperator.Equal, false)
                }
            );
            return (await response.GetNextSetAsync()).FirstOrDefault();
        }

        public async Task SaveRaceReminder(RaceReminder raceReminder)
        {
            await _dynamoDbContext.SaveAsync(raceReminder);
        }
        
        public async Task<RaceReminder> UpsertRaceReminder
        (
            int raceId,
            long athleteId,
            Action<RaceReminder> setProperties
        )
        {
            var raceReminder = await FetchExistingRaceReminder(raceId, athleteId) ??
                                      new RaceReminder {RaceId = raceId, AthleteId = athleteId};
            setProperties.Invoke(raceReminder);
            await SaveRaceReminder(raceReminder);
            return raceReminder;
        }

        private async Task<RaceReminder> FetchExistingRaceReminder(int raceId, long athleteId)
        {
            return await _dynamoDbContext.LoadAsync<RaceReminder>(raceId, athleteId);
        }
    }
}
