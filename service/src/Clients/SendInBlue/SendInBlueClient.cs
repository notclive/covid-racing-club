﻿using System.Collections.Generic;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Model;

namespace Service.Clients.SendInBlue
{
    public class SendInBlueClient
    {
        private readonly SMTPApi _smtpApi;

        public SendInBlueClient(SendInBlueConfiguration configuration)
        {
            _smtpApi = new SMTPApi(new sib_api_v3_sdk.Client.Configuration
            {
                ApiKey = new Dictionary<string, string>
                {
                    {"api-key", configuration.ApiKey}
                }
            });
        }

        public void SendTransactionalEmail(string email, string firstName, string lastName, int templateId, dynamic parameters)
        {
            parameters.FIRST_NAME = firstName;
            parameters.LAST_NAME = lastName;
            _smtpApi.SendTransacEmail(new SendSmtpEmail
            (
                to: new List<SendSmtpEmailTo> {new SendSmtpEmailTo(email, $"{firstName} {lastName}")},
                templateId: templateId,
                _params: parameters
            ));
        }
    }
}
