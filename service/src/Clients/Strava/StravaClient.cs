﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;
using Microsoft.Extensions.Logging;
using Service.Clients.Strava.Responses;

namespace Service.Clients.Strava
{
    public class StravaClient
    {
        private readonly HttpClient _client = new HttpClient();

        private readonly JsonSerializerOptions _jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };

        private readonly ILogger<StravaClient> _logger;
        private readonly StravaConfiguration _stravaConfiguration;

        public StravaClient(ILogger<StravaClient> logger, StravaConfiguration stravaConfiguration)
        {
            _logger = logger;
            _stravaConfiguration = stravaConfiguration;
        }

        public async Task<TokenResponse> ExchangeCodeForTokens(string code)
        {
            var response = await _client.RequestAuthorizationCodeTokenAsync(new AuthorizationCodeTokenRequest
            {
                Address = $"{_stravaConfiguration.Url}/oauth/token",

                ClientId = _stravaConfiguration.ClientId,
                ClientSecret = _stravaConfiguration.ClientSecret,

                Code = code,
                RedirectUri = "https://this-is-not-checked.com"
            });

            if (response.AccessToken == null)
            {
                _logger.LogError($"Failed to exchange code for token, Strava responded with: {response.Raw}");
                throw new Exception(response.Error);
            }

            return response;
        }

        public async Task<TokenResponse> RefreshTokens(string refreshToken)
        {
            var response = await _client.RequestRefreshTokenAsync(new RefreshTokenRequest
            {
                Address = $"{_stravaConfiguration.Url}/oauth/token",

                ClientId = _stravaConfiguration.ClientId,
                ClientSecret = _stravaConfiguration.ClientSecret,

                RefreshToken = refreshToken
            });

            if (response.AccessToken == null)
            {
                _logger.LogError($"Failed to refresh token, Strava responded with: {response.Raw}");
                throw new Exception(response.Error);
            }

            return response;
        }

        public async Task<StravaAthlete> FetchAthlete(string accessToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_stravaConfiguration.Url}/api/v3/athlete");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<StravaAthlete>(content, _jsonSerializerOptions);
        }

        public async Task<StravaDetailedActivity> FetchActivity(long activityId, string accessToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_stravaConfiguration.Url}/api/v3/activities/{activityId}");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<StravaDetailedActivity>(content, _jsonSerializerOptions);
        }

        public async Task<StravaActivity[]> FetchActivities(DateTime after, DateTime before, string accessToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_stravaConfiguration.Url}/api/v3/athlete/activities?after={after.ToEpochTime()}&before={before.ToEpochTime()}");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<StravaActivity[]>(content, _jsonSerializerOptions);
        }

        public async Task<StravaAltitudeStream> FetchAltitudeStream(long activityId, string accessToken)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_stravaConfiguration.Url}/api/v3/activities/{activityId}/streams?keys=altitude&key_by_type=true");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<StravaStreamSet>(content, _jsonSerializerOptions).Altitude;
        }
    }
}
