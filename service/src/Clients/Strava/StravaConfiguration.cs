﻿namespace Service.Clients.Strava
{
    public class StravaConfiguration
    {
        public string Url { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
