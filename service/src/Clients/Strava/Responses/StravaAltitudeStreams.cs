﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Service.Clients.Strava.Responses
{
    public static class StravaAltitudeStreams
    {
        // Not very accurate and doesn't match Strava.
        // Strava and other services will smooth the data first.
        public static float? CalculateElevationGainForEffort
        (
            this StravaAltitudeStream altitudeStream,
            StravaBestEffort bestEffort
        )
        {
            var dataForEffort = ExtractDataForEffort(altitudeStream, bestEffort);
            if (dataForEffort.Count == 0)
            {
                return null;
            }

            return SelectEachPairInData(dataForEffort, (a, b) => b > a ? b - a : 0).Sum();
        }

        // Not very accurate and doesn't match Strava.
        // Strava and other services will smooth the data first.
        public static float? CalculateElevationLossForEffort
        (
            this StravaAltitudeStream altitudeStream,
            StravaBestEffort bestEffort
        )
        {
            var dataForEffort = ExtractDataForEffort(altitudeStream, bestEffort);
            if (dataForEffort.Count == 0)
            {
                return null;
            }

            return SelectEachPairInData(dataForEffort, (a, b) => a > b ? a - b : 0).Sum();
        }

        private static List<float> ExtractDataForEffort
        (
            StravaAltitudeStream altitudeStream,
            StravaBestEffort bestEffort
        )
        {
            if (altitudeStream.Data.Length < bestEffort.EndIndex)
            {
                return new List<float>();
            }

            return altitudeStream.Data
                .Skip(bestEffort.StartIndex)
                // + 1 to include data at EndIndex.
                .Take(bestEffort.EndIndex + 1 - bestEffort.StartIndex)
                .ToList();
        }

        private static IEnumerable<T> SelectEachPairInData<T>(List<float> data, Func<float, float, T> select)
        {
            return data.Zip(data.Skip(1), select);
        }
    }
}
