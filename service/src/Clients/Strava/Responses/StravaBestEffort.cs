﻿using System.Text.Json.Serialization;

namespace Service.Clients.Strava.Responses
{
    public class StravaBestEffort
    {
        public string Name { get; set; }

        [JsonPropertyName("elapsed_time")] public int ElapsedTime { get; set; }

        [JsonPropertyName("start_index")] public int StartIndex { get; set; }

        [JsonPropertyName("end_index")] public int EndIndex { get; set; }
    }
}
