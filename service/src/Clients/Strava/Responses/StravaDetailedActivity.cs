﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Service.Clients.Strava.Responses
{
    public class StravaDetailedActivity : StravaActivity
    {
        [JsonPropertyName("best_efforts")] public List<StravaBestEffort> BestEfforts { get; set; }
    }
}
