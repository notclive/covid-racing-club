﻿namespace Service.Clients.Strava.Responses
{
    public class StravaAltitudeStream
    {
        public float[] Data { get; set; }
    }
}
