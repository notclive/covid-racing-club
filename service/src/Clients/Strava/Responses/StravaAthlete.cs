﻿namespace Service.Clients.Strava.Responses
{
    public class StravaAthlete
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Sex { get; set; }
    }
}
