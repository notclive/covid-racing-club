﻿namespace Service.Clients.Strava.Responses
{
    public class StravaStreamSet
    {
        public StravaAltitudeStream Altitude { get; set; }
    }
}
