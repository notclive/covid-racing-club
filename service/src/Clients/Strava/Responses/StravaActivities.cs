﻿using System.Linq;
using Service.Models.Response;
using Service.Models.Static.RaceDistance;

namespace Service.Clients.Strava.Responses
{
    public static class StravaActivities
    {
        public static int? TryDetermineRaceTimeInSeconds
        (
            this StravaDetailedActivity stravaActivity,
            RaceDistance raceDistance
        )
        {
            return FindBestEffortMatchingRaceDistance(stravaActivity, raceDistance)?.ElapsedTime;
        }

        public static StravaBestEffort FindBestEffortMatchingRaceDistance
        (
            this StravaDetailedActivity stravaActivity,
            RaceDistance raceDistance
        )
        {
            return stravaActivity.BestEfforts
                .FirstOrDefault(e => e.Name == raceDistance.GetNameUsedByStrava());
        }

        public static Eligibility DetermineEligibilityForRace
        (
            this StravaActivity stravaActivity,
            RaceDistance raceDistance
        )
        {
            if (stravaActivity.Type != "Run")
            {
                return Eligibility.NotRun;
            }

            if (stravaActivity.Distance < raceDistance.GetDistanceInMeters())
            {
                return Eligibility.TooShort;
            }

            return Eligibility.Eligible;
        }
    }
}
