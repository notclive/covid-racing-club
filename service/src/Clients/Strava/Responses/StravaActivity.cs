﻿using System;
using System.Text.Json.Serialization;

namespace Service.Clients.Strava.Responses
{
    public class StravaActivity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        [JsonPropertyName("start_date")] public DateTime StartDate { get; set; }
        public double Distance { get; set; }
        public string Type { get; set; }
    }
}
