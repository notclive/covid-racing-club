﻿using System.ComponentModel.DataAnnotations;

namespace Service.Models.Request
{
    public class PostRaceReminderRequest
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
    }
}
