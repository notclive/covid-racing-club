﻿using System.ComponentModel.DataAnnotations;

namespace Service.Models.Request
{
    public class PostRaceSubmissionRequest
    {
        [Required]
        public long ActivityId { get; set; }
    }
}
