﻿using System.ComponentModel.DataAnnotations;

namespace Service.Models.Request
{
    public class PostResultsNotificationRequest
    {
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; }
    }
}
