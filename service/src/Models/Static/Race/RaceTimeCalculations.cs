﻿using System;

namespace Service.Models.Static.Race
{
    public static class RaceTimeCalculations
    {
        public static DateTime GetEarliestTimeRaceCanBeRun(this Database.Race race)
        {
            var startOfRacingDay = race.Date;
            return startOfRacingDay.AddHours(-12);
        }

        public static DateTime GetLatestTimeRaceCanBeRun(this Database.Race race)
        {
            var endOfRacingDay = race.Date.AddDays(1);
            return endOfRacingDay.AddHours(12);
        }

        public static DateTime GetTimeThatResultsAreReleased(this Database.Race race)
        {
            return race.GetLatestTimeRaceCanBeRun().AddDays(1);
        }
    }
}
