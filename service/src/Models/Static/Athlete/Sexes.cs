﻿using Service.Clients.Strava.Responses;

namespace Service.Models.Static.Athlete
{
    public static class Sexes
    {
        public static Sex? FromStravaAthlete(StravaAthlete stravaAthlete)
        {
            switch (stravaAthlete.Sex)
            {
                case "M":
                    return Sex.Male;
                case "F":
                    return Sex.Female;
                default:
                    return null;
            }
        }
    }
}
