﻿using System;
using System.Reflection;

namespace Service.Models.Static.RaceDistance
{
    public static class RaceDistances
    {
        public static string GetNameUsedByStrava(this RaceDistance raceDistance)
        {
            var attribute = GetRaceDistanceAttribute(raceDistance);
            return attribute.NameUsedByStrava;
        }

        public static string GetUserFriendlyDescription(this RaceDistance raceDistance)
        {
            var attribute = GetRaceDistanceAttribute(raceDistance);
            return attribute.UserFriendlyDescription;
        }

        public static int GetDistanceInMeters(this RaceDistance raceDistance)
        {
            var attribute = GetRaceDistanceAttribute(raceDistance);
            return attribute.DistanceInMeters;
        }

        private static RaceDistanceAttribute GetRaceDistanceAttribute(RaceDistance raceDistance)
        {
            return (RaceDistanceAttribute)Attribute.GetCustomAttribute
            (
                ReflectValue(raceDistance),
                typeof(RaceDistanceAttribute)
            );
        }

        private static MemberInfo ReflectValue(RaceDistance raceDistance)
        {
            return typeof(RaceDistance).GetField(Enum.GetName(typeof(RaceDistance), raceDistance));
        }
    }
}
