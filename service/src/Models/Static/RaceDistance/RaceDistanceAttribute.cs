﻿using System;

namespace Service.Models.Static.RaceDistance
{
    public class RaceDistanceAttribute : Attribute
    {
        public RaceDistanceAttribute(string nameUsedByStrava, string userFriendlyDescription, int distanceInMeters)
        {
            NameUsedByStrava = nameUsedByStrava;
            UserFriendlyDescription = userFriendlyDescription;
            DistanceInMeters = distanceInMeters;
        }

        public string NameUsedByStrava { get; }

        // Should fit into the sentence "Go and run ${UserFriendlyDescription}".
        public string UserFriendlyDescription { get; }

        public int DistanceInMeters { get; }
    }
}
