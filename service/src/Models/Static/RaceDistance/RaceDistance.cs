﻿namespace Service.Models.Static.RaceDistance
{
    public enum RaceDistance
    {
        [RaceDistance("1/2 mile", "800 meters", 800)]
        HalfMile,
        [RaceDistance("1 mile", "1 mile", 1609)]
        OneMile,
        [RaceDistance("5k",  "5 kilometers", 5000)]
        FiveKilometers,
        [RaceDistance("10k", "10 kilometers", 10000)]
        TenKilometers,
        [RaceDistance("10 mile", "10 miles", 16090)]
        TenMiles,
        [RaceDistance("Half-Marathon",  "a half-marathon", 21090)]
        HalfMarathon,
        [RaceDistance("Marathon", "a marathon", 42180)]
        Marathon,
        [RaceDistance("50k", "50 kilometers", 50000)]
        FiftyKilometers
    }
}
