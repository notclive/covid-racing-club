﻿using System;
using Amazon.DynamoDBv2.DataModel;
using Service.Models.Static.RaceDistance;

namespace Service.Models.Database
{
    [DynamoDBTable("Race")]
    public class Race
    {
        [DynamoDBHashKey] public int RaceId { get; set; }
        [DynamoDBProperty] public DateTime Date { get; set; }
        [DynamoDBProperty] public string Name { get; set; }
        [DynamoDBProperty] public string Description { get; set; }

        [DynamoDBProperty(typeof(EnumAsStringConverter<RaceDistance>))]
        public RaceDistance RaceDistance { get; set; }
    }
}
