using Amazon.DynamoDBv2.DataModel;
using Service.Models.Static.Athlete;

namespace Service.Models.Database
{
    [DynamoDBTable("RaceSubmission")]
    public class RaceSubmission
    {
        [DynamoDBHashKey] public int RaceId { get; set; }
        [DynamoDBRangeKey] public long AthleteId { get; set; }
        [DynamoDBProperty] public long ActivityId { get; set; }
        [DynamoDBProperty] public string FirstName { get; set; }
        [DynamoDBProperty] public string LastName { get; set; }
        [DynamoDBProperty(typeof(EnumAsStringConverter<Sex>))] public Sex? Sex { get; set; }
        [DynamoDBProperty] public int? RaceTimeInSeconds { get; set; }
        [DynamoDBProperty] public float? ElevationGainInMeters { get; set; }
        [DynamoDBProperty] public float? ElevationLossInMeters { get; set; }
    }
}
