﻿using System;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace Service.Models.Database
{
    public class EnumAsStringConverter<TEnum> : IPropertyConverter where TEnum : struct
    {
        public DynamoDBEntry ToEntry(object value)
        {
            return new Primitive(value.ToString());
        }

        public object FromEntry(DynamoDBEntry entry)
        {
            return Enum.Parse<TEnum>(entry.AsString());
        }
    }
}
