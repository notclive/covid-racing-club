﻿using Amazon.DynamoDBv2.DataModel;
using Service.Models.Static.Athlete;

namespace Service.Models.Database
{
    [DynamoDBTable("Athlete")]
    public class Athlete
    {
        [DynamoDBHashKey] public long AthleteId { get; set; }
        [DynamoDBProperty] public string FirstName { get; set; }
        [DynamoDBProperty] public string LastName { get; set; }
        [DynamoDBProperty(typeof(EnumAsStringConverter<Sex>))] public Sex? Sex { get; set; }
        [DynamoDBProperty] public string EmailAddress { get; set; }
        [DynamoDBProperty] public string AccessToken { get; set; }
        [DynamoDBProperty] public string RefreshToken { get; set; }
        [DynamoDBVersion] public int? Version { get; set; }
    }
}
