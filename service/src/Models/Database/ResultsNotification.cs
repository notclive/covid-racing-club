﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace Service.Models.Database
{
    [DynamoDBTable("ResultsNotification")]
    public class ResultsNotification
    {
        [DynamoDBHashKey] public int RaceId { get; set; }
        [DynamoDBProperty] public DateTime TimeThatResultsAreReleased { get; set; }
        [DynamoDBProperty] public bool NotificationHasBeenSent { get; set; }
        [DynamoDBRangeKey] public long AthleteId { get; set; }
        [DynamoDBProperty] public string EmailAddress { get; set; }
        [DynamoDBProperty] public string FirstName { get; set; }
        [DynamoDBProperty] public string LastName { get; set; }
        [DynamoDBVersion] public int? Version { get; set; }
    }
}
