﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace Service.Models.Database
{
    [DynamoDBTable("RaceReminder")]
    public class RaceReminder
    {
        [DynamoDBHashKey] public int RaceId { get; set; }
        [DynamoDBProperty] public DateTime TimeThatRaceCanBeRun { get; set; }
        [DynamoDBProperty] public bool ReminderHasBeenSent { get; set; }
        [DynamoDBRangeKey] public long AthleteId { get; set; }
        [DynamoDBProperty] public string EmailAddress { get; set; }
        [DynamoDBProperty] public string FirstName { get; set; }
        [DynamoDBProperty] public string LastName { get; set; }
        [DynamoDBVersion] public int? Version { get; set; }
    }
}
