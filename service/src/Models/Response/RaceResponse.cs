﻿using System;
using Service.Models.Database;
using Service.Models.Static.Race;
using Service.Models.Static.RaceDistance;

namespace Service.Models.Response
{
    public class RaceResponse
    {
        public int RaceId { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DistanceInMeters { get; set; }
        public DateTime EarliestTimeRaceCanBeRun { get; set; }
        public DateTime LatestTimeRaceCanBeRun { get; set; }
        public DateTime TimeThatResultsAreReleased { get; set; }

        public static RaceResponse FromRace(Race race)
        {
            return new RaceResponse
            {
                RaceId = race.RaceId,
                Date = race.Date,
                Name = race.Name,
                Description = race.Description,
                DistanceInMeters = race.RaceDistance.GetDistanceInMeters(),
                EarliestTimeRaceCanBeRun = race.GetEarliestTimeRaceCanBeRun(),
                LatestTimeRaceCanBeRun = race.GetLatestTimeRaceCanBeRun(),
                TimeThatResultsAreReleased = race.GetTimeThatResultsAreReleased()
            };
        }
    }
}
