﻿using IdentityModel.Client;
using Service.Clients.Strava.Responses;
using Service.Models.Database;

namespace Service.Models.Response
{
    public class AthleteResponse
    {
        public long AthleteId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string AccessToken { get; set; }

        public static AthleteResponse FromAthlete(Athlete athlete)
        {
            return new AthleteResponse
            {
                AthleteId = athlete.AthleteId,
                FirstName = athlete.FirstName,
                LastName = athlete.LastName,
                EmailAddress = athlete.EmailAddress,
                AccessToken = athlete.AccessToken
            };
        }
    }
}
