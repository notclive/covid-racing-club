﻿using Service.Models.Database;
using Service.Models.Static.Athlete;

namespace Service.Models.Response
{
    public class SubmissionResponse
    {
        public int RaceId { get; set; }
        public long AthleteId { get; set; }
        public long ActivityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Sex? Sex { get; set; }
        public long? RaceTimeInSeconds { get; set; }
        public float? ElevationGainInMeters { get; set; }
        public float? ElevationLossInMeters { get; set; }

        public static SubmissionResponse FromSubmission(RaceSubmission submission)
        {
            return new SubmissionResponse
            {
                RaceId = submission.RaceId,
                AthleteId = submission.AthleteId,
                ActivityId = submission.ActivityId,
                FirstName = submission.FirstName,
                LastName = submission.LastName,
                Sex = submission.Sex,
                RaceTimeInSeconds = submission.RaceTimeInSeconds,
                ElevationGainInMeters = submission.ElevationGainInMeters,
                ElevationLossInMeters = submission.ElevationLossInMeters
            };
        }
    }
}
