﻿using System;
using Service.Clients.Strava.Responses;
using Service.Models.Static.RaceDistance;

namespace Service.Models.Response
{
    public class PotentialSubmissionResponse
    {
        public long ActivityId { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public Eligibility Eligibility { get; set; }
        public int? RaceTimeInSeconds { get; set; }

        public static PotentialSubmissionResponse FromStravaActivityAndRaceDistance
        (
            StravaActivity stravaActivity,
            RaceDistance raceDistance
        )
        {
            return new PotentialSubmissionResponse
            {
                ActivityId = stravaActivity.Id,
                Name = stravaActivity.Name,
                StartTime = stravaActivity.StartDate,
                Eligibility = stravaActivity.DetermineEligibilityForRace(raceDistance)
            };
        }
    }
}
