﻿namespace Service.Models.Response
{
    public enum Eligibility
    {
        Eligible,
        NotRun,
        TooShort
    }
}
