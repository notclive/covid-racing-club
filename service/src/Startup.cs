﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using Service.Asynchronous;
using Service.Clients.SendInBlue;
using Service.Clients.Strava;
using Service.Services;

namespace Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddTransient<TimeLimitedRepeater>()
                .AddTransient<DueResultsNotificationSender>()
                .AddTransient<DueRaceReminderSender>();

            ConfigureSendInBlue(services);
            ConfigureStrava(services);
            ConfigureDynamoDb(services);

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions
                (
                    o => o.SerializerSettings.Converters.Add(new StringEnumConverter())
                );
        }

        private static void ConfigureSendInBlue(IServiceCollection services)
        {
            var sendInBlueSection = Configuration.GetSection("SendInBlue");

            services
                .AddTransient<SendInBlueClient>()
                .AddSingleton
                (
                    new SendInBlueConfiguration
                    {
                        ApiKey = sendInBlueSection["ApiKey"]
                    }
                );
        }

        private static void ConfigureStrava(IServiceCollection services)
        {
            var stravaConfig = Configuration.GetSection("Strava");

            services
                .AddTransient<StravaClient>()
                .AddSingleton
                (
                    new StravaConfiguration
                    {
                        Url = stravaConfig["Url"],
                        ClientId = stravaConfig["ClientId"],
                        ClientSecret = stravaConfig["ClientSecret"]
                    }
                );
        }

        private static void ConfigureDynamoDb(IServiceCollection services)
        {
            var dynamoDbConfig = Configuration.GetSection("DynamoDb");
            var useLocalDatabase = dynamoDbConfig.GetValue<bool>("Local");

            if (useLocalDatabase)
            {
                var serviceUrl = dynamoDbConfig.GetValue<string>("LocalServiceUrl");
                var clientConfig = new AmazonDynamoDBConfig {ServiceURL = serviceUrl};
                var localClient = new AmazonDynamoDBClient(clientConfig);
                services.AddSingleton<IAmazonDynamoDB>(localClient);
            }
            else
            {
                services.AddAWSService<IAmazonDynamoDB>();
            }

            services
                .AddTransient<RaceService>()
                .AddTransient<AthleteService>()
                .AddTransient<RaceSubmissionService>()
                .AddTransient<ResultsNotificationService>()
                .AddTransient<RaceReminderService>()
                .AddTransient<IDynamoDBContext, DynamoDBContext>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(p => p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
