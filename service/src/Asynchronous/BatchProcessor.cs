﻿using System.Threading.Tasks;

namespace Service.Asynchronous
{
    public interface IBatchProcessor
    {
        Task<BatchProcessingResult> ProcessBatchOfWork();
    }
}
