﻿using System.Dynamic;
using System.Threading.Tasks;
using Service.Clients.SendInBlue;
using Service.Models.Database;
using Service.Services;

namespace Service.Asynchronous
{
    public class DueResultsNotificationSender : IBatchProcessor
    {
        private const int ResultsNotificationTemplateId = 2;

        private readonly SendInBlueClient _sendInBlueClient;
        private readonly RaceService _raceService;
        private readonly ResultsNotificationService _resultsNotificationService;

        public DueResultsNotificationSender
        (
            SendInBlueClient sendInBlueClient,
            RaceService raceService,
            ResultsNotificationService resultsNotificationService
        )
        {
            _sendInBlueClient = sendInBlueClient;
            _raceService = raceService;
            _resultsNotificationService = resultsNotificationService;
        }

        public async Task<BatchProcessingResult> ProcessBatchOfWork()
        {
            var dueResultsNotification = await _resultsNotificationService.FetchDueResultsNotification();
            if (dueResultsNotification == null)
            {
                return BatchProcessingResult.NoMoreWorkToDo;
            }

            await MarkNotificationAsSent(dueResultsNotification);
            var race = await _raceService.FetchRace(dueResultsNotification.RaceId);
            SendEmail(dueResultsNotification, race);
            return BatchProcessingResult.PossiblyMoreWorkToDo;
        }

        private async Task MarkNotificationAsSent(ResultsNotification resultsNotification)
        {
            // If another processor marks the notification as sent an exception will be thrown due to optimistic locking.
            // This prevents more than one email from being sent. 
            resultsNotification.NotificationHasBeenSent = true;
            await _resultsNotificationService.SaveResultsNotification(resultsNotification);
        }

        private void SendEmail(ResultsNotification resultsNotification, Race race)
        {
            dynamic parameters = new ExpandoObject();
            parameters.DAY_OF_WEEK = race.Date.DayOfWeek.ToString();
            parameters.RACE_NAME = race.Name;
            parameters.RACE_ID = race.RaceId;
            _sendInBlueClient.SendTransactionalEmail
            (
                resultsNotification.EmailAddress,
                resultsNotification.FirstName,
                resultsNotification.LastName,
                ResultsNotificationTemplateId,
                parameters
            );
        }
    }
}
