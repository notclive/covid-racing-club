﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Service.Asynchronous
{
    public class TimeLimitedRepeater
    {
        private readonly ILogger<TimeLimitedRepeater> _logger;

        public TimeLimitedRepeater(ILogger<TimeLimitedRepeater> logger)
        {
            _logger = logger;
        }

        public TimeLimitedRepeaterWithProcessor Execute(IBatchProcessor batchProcessor)
        {
            return new TimeLimitedRepeaterWithProcessor(_logger, batchProcessor);
        }

        public class TimeLimitedRepeaterWithProcessor
        {
            private readonly ILogger<TimeLimitedRepeater> _logger;
            private readonly IBatchProcessor _batchProcessor;

            public TimeLimitedRepeaterWithProcessor(ILogger<TimeLimitedRepeater> logger, IBatchProcessor batchProcessor)
            {
                _logger = logger;
                _batchProcessor = batchProcessor;
            }

            public async Task ForUpTo(TimeSpan duration)
            {
                var endTime = DateTime.Now.Add(duration);
                while (DateTime.Now < endTime)
                {
                    try
                    {
                        var batchProcessingResult = await _batchProcessor.ProcessBatchOfWork();
                        if (batchProcessingResult == BatchProcessingResult.NoMoreWorkToDo)
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Error while processing batch of work.");
                    }
                }
            }
        }
    }
}
