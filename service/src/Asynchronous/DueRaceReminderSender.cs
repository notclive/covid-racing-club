﻿using System.Dynamic;
using System.Threading.Tasks;
using Service.Clients.SendInBlue;
using Service.Models.Database;
using Service.Models.Static.RaceDistance;
using Service.Services;

namespace Service.Asynchronous
{
    public class DueRaceReminderSender : IBatchProcessor
    {
        private const int RaceReminderTemplateId = 3;

        private readonly SendInBlueClient _sendInBlueClient;
        private readonly RaceService _raceService;
        private readonly RaceReminderService _raceReminderService;

        public DueRaceReminderSender
        (
            SendInBlueClient sendInBlueClient,
            RaceService raceService,
            RaceReminderService raceReminderService
        )
        {
            _sendInBlueClient = sendInBlueClient;
            _raceService = raceService;
            _raceReminderService = raceReminderService;
        }

        public async Task<BatchProcessingResult> ProcessBatchOfWork()
        {
            var dueRaceReminder = await _raceReminderService.FetchDueRaceReminder();
            if (dueRaceReminder == null)
            {
                return BatchProcessingResult.NoMoreWorkToDo;
            }

            await MarkReminderAsSent(dueRaceReminder);
            var race = await _raceService.FetchRace(dueRaceReminder.RaceId);
            SendEmail(dueRaceReminder, race);
            return BatchProcessingResult.PossiblyMoreWorkToDo;
        }

        private async Task MarkReminderAsSent(RaceReminder resultsNotification)
        {
            // If another processor marks the reminder as sent an exception will be thrown due to optimistic locking.
            // This prevents more than one email from being sent. 
            resultsNotification.ReminderHasBeenSent = true;
            await _raceReminderService.SaveRaceReminder(resultsNotification);
        }

        private void SendEmail(RaceReminder raceReminder, Race race)
        {
            dynamic parameters = new ExpandoObject();
            parameters.DAY_OF_WEEK = race.Date.DayOfWeek.ToString();
            parameters.RACE_DISTANCE = race.RaceDistance.GetUserFriendlyDescription();
            parameters.RACE_ID = race.RaceId;
            _sendInBlueClient.SendTransactionalEmail
            (
                raceReminder.EmailAddress,
                raceReminder.FirstName,
                raceReminder.LastName,
                RaceReminderTemplateId,
                parameters
            );
        }
    }
}
