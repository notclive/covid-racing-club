namespace Service.Asynchronous
{
    public enum BatchProcessingResult
    {
        NoMoreWorkToDo,
        PossiblyMoreWorkToDo,
    }
}
