using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Asynchronous;
using Service.Clients.Strava;
using Service.Models.Database;
using Service.Models.Request;
using Service.Models.Static.Race;
using Service.Services;

namespace Service.Controllers.RaceReminders
{
    public class RaceReminderController : Controller
    {
        private readonly StravaClient _stravaClient;
        private readonly RaceService _raceService;
        private readonly AthleteService _athleteService;
        private readonly RaceReminderService _raceReminderService;
        private readonly TimeLimitedRepeater _timeLimitedRepeater;
        private readonly DueRaceReminderSender _dueRaceReminderSender;

        public RaceReminderController
        (
            StravaClient stravaClient,
            RaceService raceService,
            AthleteService athleteService,
            RaceReminderService raceReminderService,
            TimeLimitedRepeater timeLimitedRepeater,
            DueRaceReminderSender dueRaceReminderSender
        )
        {
            _stravaClient = stravaClient;
            _raceService = raceService;
            _athleteService = athleteService;
            _raceReminderService = raceReminderService;
            _timeLimitedRepeater = timeLimitedRepeater;
            _dueRaceReminderSender = dueRaceReminderSender;
        }

        [HttpPost]
        [Route("races/{raceId}/race-reminders")]
        public async Task<IActionResult> PostRaceReminder
        (
            [FromRoute] int raceId,
            [FromQuery] string token,
            [FromBody] PostRaceReminderRequest request
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var race = _raceService.FetchRace(raceId);
            var athlete = PersistAthletesContactInformation(token, request);
            await Task.WhenAll(race, athlete);
            await PersistRaceReminder(race.Result, athlete.Result);
            return NoContent();
        }

        private async Task<Athlete> PersistAthletesContactInformation
        (
            string token,
            PostRaceReminderRequest request
        )
        {
            var stravaAthlete = await _stravaClient.FetchAthlete(token);
            return await _athleteService.UpsertAthlete(stravaAthlete.Id, athlete =>
            {
                athlete.FirstName = stravaAthlete.FirstName;
                athlete.LastName = stravaAthlete.LastName;
                athlete.EmailAddress = request.EmailAddress;
            });
        }

        private async Task PersistRaceReminder(Race race, Athlete athlete)
        {
            await _raceReminderService.UpsertRaceReminder
            (
                race.RaceId,
                athlete.AthleteId,
                resultsNotification =>
                {
                    resultsNotification.TimeThatRaceCanBeRun = race.GetEarliestTimeRaceCanBeRun();
                    resultsNotification.ReminderHasBeenSent = false;
                    resultsNotification.EmailAddress = athlete.EmailAddress;
                    resultsNotification.FirstName = athlete.FirstName;
                    resultsNotification.LastName = athlete.LastName;
                }
            );
        }

        [HttpPost]
        [Route("race-reminders/send-due")]
        public async Task<IActionResult> SendDueRaceReminders()
        {
            await _timeLimitedRepeater
                .Execute(_dueRaceReminderSender)
                .ForUpTo(TimeSpan.FromSeconds(25));
            return NoContent();
        }
    }
}
