﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Models.Response;
using Service.Services;

namespace Service.Controllers.Races
{
    public class RaceController : Controller
    {
        private const int OneHourInSeconds = 60 * 60;
        
        private readonly RaceService _raceService;

        public RaceController(RaceService raceService)
        {
            _raceService = raceService;
        }

        [HttpGet]
        [Route("races/{raceId}")]
        [ResponseCache(Duration = OneHourInSeconds)]
        public async Task<RaceResponse> GetRace([FromRoute] int raceId)
        {
            var race = await _raceService.FetchRace(raceId);
            return RaceResponse.FromRace(race);
        }

        [HttpGet]
        [Route("races")]
        [ResponseCache(Duration = OneHourInSeconds)]
        public async Task<IActionResult> GetRaces
        (
            [FromQuery] [Required] DateTime after,
            [FromQuery] [Required] DateTime before
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var races = await _raceService.FetchRacesBetweenDates(after, before);
            return Ok(races.Select(RaceResponse.FromRace));
        }
    }
}
