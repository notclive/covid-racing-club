﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Clients.Strava;
using Service.Clients.Strava.Responses;
using Service.Models.Database;
using Service.Models.Request;
using Service.Models.Response;
using Service.Models.Static.Athlete;
using Service.Services;

namespace Service.Controllers.Races
{
    public class RaceSubmissionsController : Controller
    {
        private const int OneHourInSeconds = 60 * 60;

        private readonly StravaClient _stravaClient;
        private readonly RaceService _raceService;
        private readonly RaceSubmissionService _raceSubmissionService;
        private readonly ILogger<RaceSubmissionsController> _logger;

        public RaceSubmissionsController
        (
            ILogger<RaceSubmissionsController> logger,
            StravaClient stravaClient,
            RaceService raceService,
            RaceSubmissionService raceSubmissionService
        )
        {
            _logger = logger;
            _stravaClient = stravaClient;
            _raceService = raceService;
            _raceSubmissionService = raceSubmissionService;
        }

        [HttpGet]
        [Route("races/{raceId}/submissions")]
        [ResponseCache(Duration = OneHourInSeconds)]
        public async Task<IEnumerable<SubmissionResponse>> GetSubmissions([FromRoute] int raceId)
        {
            var submissions = await _raceSubmissionService.FetchSubmissionsForRace(raceId);
            return submissions.Select(SubmissionResponse.FromSubmission);
        }

        [HttpPost]
        [Route("races/{raceId}/submissions")]
        public async Task<IActionResult> PostSubmission
        (
            [FromRoute] int raceId,
            [FromQuery] string token,
            [FromBody] PostRaceSubmissionRequest request
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var athlete = await _stravaClient.FetchAthlete(token);
            var optionalProperties = await TryFetchOptionalSubmissionProperties(token, raceId, request.ActivityId);
            await _raceSubmissionService.SaveSubmission(new RaceSubmission
            {
                RaceId = raceId,
                AthleteId = athlete.Id,
                ActivityId = request.ActivityId,
                FirstName = athlete.FirstName,
                LastName = athlete.LastName,
                Sex = Sexes.FromStravaAthlete(athlete),
                RaceTimeInSeconds = optionalProperties?.RaceTimeInSeconds,
                ElevationGainInMeters = optionalProperties?.ElevationGainInMeters,
                ElevationLossInMeters = optionalProperties?.ElevationLossInMeters
            });
            return NoContent();
        }

        private async Task<OptionalProperties> TryFetchOptionalSubmissionProperties
        (
            string token,
            int raceId,
            long activityId
        )
        {
            try
            {
                var race = _raceService.FetchRace(raceId);
                var stravaActivity = _stravaClient.FetchActivity(activityId, token);
                var streamSet = _stravaClient.FetchAltitudeStream(activityId, token);
                await Task.WhenAll(race, stravaActivity, streamSet);

                var bestEffort = stravaActivity.Result.FindBestEffortMatchingRaceDistance(race.Result.RaceDistance);

                return new OptionalProperties
                {
                    RaceTimeInSeconds = bestEffort?.ElapsedTime,
                    ElevationGainInMeters = streamSet.Result.CalculateElevationGainForEffort(bestEffort),
                    ElevationLossInMeters = streamSet.Result.CalculateElevationLossForEffort(bestEffort)
                };
            }
            catch (Exception e)
            {
                // The above code is known to fail for manual Strava activities, which won't have best efforts.
                _logger.LogError(e, "Failed to extend race submission with optional properties.");
                return new OptionalProperties();
            }
        }

        private class OptionalProperties
        {
            public int? RaceTimeInSeconds { get; set; }
            public float? ElevationGainInMeters { get; set; }
            public float? ElevationLossInMeters { get; set; }
        }
    }
}
