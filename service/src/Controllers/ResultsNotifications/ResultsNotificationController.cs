using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Asynchronous;
using Service.Clients.Strava;
using Service.Models.Database;
using Service.Models.Request;
using Service.Models.Static.Race;
using Service.Services;

namespace Service.Controllers.ResultsNotifications
{
    public class ResultsNotificationController : Controller
    {
        private readonly StravaClient _stravaClient;
        private readonly RaceService _raceService;
        private readonly AthleteService _athleteService;
        private readonly ResultsNotificationService _resultsNotificationService;
        private readonly TimeLimitedRepeater _timeLimitedRepeater;
        private readonly DueResultsNotificationSender _dueResultsNotificationSender;

        public ResultsNotificationController
        (
            StravaClient stravaClient,
            RaceService raceService,
            AthleteService athleteService,
            ResultsNotificationService resultsNotificationService,
            TimeLimitedRepeater timeLimitedRepeater,
            DueResultsNotificationSender dueResultsNotificationSender
        )
        {
            _stravaClient = stravaClient;
            _raceService = raceService;
            _athleteService = athleteService;
            _resultsNotificationService = resultsNotificationService;
            _timeLimitedRepeater = timeLimitedRepeater;
            _dueResultsNotificationSender = dueResultsNotificationSender;
        }

        [HttpPost]
        [Route("races/{raceId}/results-notifications")]
        public async Task<IActionResult> PostResultsNotification
        (
            [FromRoute] int raceId,
            [FromQuery] string token,
            [FromBody] PostResultsNotificationRequest request
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var race = _raceService.FetchRace(raceId);
            var athlete = PersistAthletesContactInformation(token, request);
            await Task.WhenAll(race, athlete);
            await PersistResultsNotification(race.Result, athlete.Result);
            return NoContent();
        }

        private async Task<Athlete> PersistAthletesContactInformation
        (
            string token,
            PostResultsNotificationRequest request
        )
        {
            var stravaAthlete = await _stravaClient.FetchAthlete(token);
            return await _athleteService.UpsertAthlete(stravaAthlete.Id, athlete =>
            {
                athlete.FirstName = stravaAthlete.FirstName;
                athlete.LastName = stravaAthlete.LastName;
                athlete.EmailAddress = request.EmailAddress;
            });
        }

        private async Task PersistResultsNotification(Race race, Athlete athlete)
        {
            await _resultsNotificationService.UpsertResultsNotification
            (
                race.RaceId,
                athlete.AthleteId,
                resultsNotification =>
                {
                    resultsNotification.TimeThatResultsAreReleased = race.GetTimeThatResultsAreReleased();
                    resultsNotification.NotificationHasBeenSent = false;
                    resultsNotification.EmailAddress = athlete.EmailAddress;
                    resultsNotification.FirstName = athlete.FirstName;
                    resultsNotification.LastName = athlete.LastName;
                }
            );
        }

        [HttpPost]
        [Route("results-notifications/send-due")]
        public async Task<IActionResult> SendDueResultsNotifications()
        {
            await _timeLimitedRepeater
                .Execute(_dueResultsNotificationSender)
                .ForUpTo(TimeSpan.FromSeconds(25));
            return NoContent();
        }
    }
}
