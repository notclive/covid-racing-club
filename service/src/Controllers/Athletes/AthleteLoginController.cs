﻿using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;
using Service.Clients.Strava;
using Service.Clients.Strava.Responses;
using Service.Models.Database;
using Service.Models.Response;
using Service.Models.Static.Athlete;
using Service.Services;

namespace Service.Controllers.Athletes
{
    public class AthleteLoginController : Controller
    {
        private readonly StravaClient _stravaClient;
        private readonly AthleteService _athleteService;

        public AthleteLoginController(StravaClient stravaClient, AthleteService athleteService)
        {
            _stravaClient = stravaClient;
            _athleteService = athleteService;
        }

        [HttpPost]
        [Route("athletes/login")]
        public async Task<AthleteResponse> Login([FromQuery] string code)
        {
            var tokens = await _stravaClient.ExchangeCodeForTokens(code);
            var stravaAthlete = await _stravaClient.FetchAthlete(tokens.AccessToken);
            var athlete = await PersistAthletesNameAndTokens(stravaAthlete, tokens);
            return AthleteResponse.FromAthlete(athlete);
        }

        private async Task<Athlete> PersistAthletesNameAndTokens(StravaAthlete stravaAthlete, TokenResponse tokens)
        {
            return await _athleteService.UpsertAthlete(stravaAthlete.Id, athlete =>
            {
                athlete.FirstName = stravaAthlete.FirstName;
                athlete.LastName = stravaAthlete.LastName;
                athlete.Sex = Sexes.FromStravaAthlete(stravaAthlete);
                athlete.RefreshToken = tokens.RefreshToken;
                athlete.AccessToken = tokens.AccessToken;
            });
        }
    }
}
