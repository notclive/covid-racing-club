﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Service.Clients.Strava;
using Service.Clients.Strava.Responses;
using Service.Models.Response;
using Service.Models.Static.Race;
using Service.Models.Static.RaceDistance;
using Service.Services;

namespace Service.Controllers.Athletes
{
    public class AthletePotentialSubmissionsController : Controller
    {
        private readonly RaceService _raceService;
        private readonly StravaClient _stravaClient;
        private readonly ILogger<AthletePotentialSubmissionsController> _logger;

        public AthletePotentialSubmissionsController
        (
            RaceService raceService,
            StravaClient stravaClient,
            ILogger<AthletePotentialSubmissionsController> logger
        )
        {
            _raceService = raceService;
            _stravaClient = stravaClient;
            _logger = logger;
        }

        [HttpGet]
        [Route("athletes/{athleteId}/potential-submissions")]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> GetPotentialSubmissions
        (
            [FromQuery] [Required] int raceId,
            [FromQuery] [Required] string token
        )
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var race = await _raceService.FetchRace(raceId);
            var activities = await _stravaClient.FetchActivities
            (
                race.GetEarliestTimeRaceCanBeRun(),
                race.GetLatestTimeRaceCanBeRun(),
                token
            );
            var potentialSubmissions = activities.Select
            (
                a => PotentialSubmissionResponse.FromStravaActivityAndRaceDistance(a, race.RaceDistance)
            ).ToList();
            await TryToEnhanceEligibleSubmissionsWithRaceTime(potentialSubmissions, race.RaceDistance, token);
            return Ok(potentialSubmissions);
        }

        private async Task TryToEnhanceEligibleSubmissionsWithRaceTime
        (
            List<PotentialSubmissionResponse> potentialSubmissions,
            RaceDistance raceDistance,
            string token
        )
        {
            await Task.WhenAll
            (
                potentialSubmissions
                    .Where(ps => ps.Eligibility == Eligibility.Eligible)
                    .Select(ps => TryToEnhanceSubmissionWithRaceTime(ps, raceDistance, token))
            );
        }

        private async Task TryToEnhanceSubmissionWithRaceTime
        (
            PotentialSubmissionResponse potentialSubmission,
            RaceDistance raceDistance,
            string token
        )
        {
            try
            {
                var detailedStravaActivity = await _stravaClient.FetchActivity(potentialSubmission.ActivityId, token);
                potentialSubmission.RaceTimeInSeconds =
                    detailedStravaActivity.TryDetermineRaceTimeInSeconds(raceDistance);
            }
            catch (Exception e)
            {
                // The above code is known to fail for manual Strava activities, which won't have best efforts.
                _logger.LogError(e, "Failed to determine race time for potential submission.");
            }
        }
    }
}
