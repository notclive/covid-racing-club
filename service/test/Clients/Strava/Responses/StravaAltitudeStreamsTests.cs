﻿using Service.Clients.Strava.Responses;
using Xunit;

namespace src.Tests.Clients.Strava.Responses
{
    public class StravaAltitudeStreamsTests
    {
        [Fact]
        public void GivenAltitudeStreamThatUndulatesWhenElevationGainIsCalculatedThenElevationLossIsIgnored()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {0.0f, 10.0f, 0.0f, 10.0f, 0.0f, 10.0f, 5.0f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 0,
                EndIndex = 6
            };

            // when
            var elevationGain = undulatingStream.CalculateElevationGainForEffort(bestEffort);

            // then
            Assert.Equal(30, elevationGain);
        }

        [Fact]
        public void GivenAltitudeStreamThatDescendsWhenElevationGainIsCalculatedThenEveryGapIsIgnored()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {100.0f, 80.0f, 60.0f, 40.0f, 20.0f, 0.0f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 0,
                EndIndex = 5
            };

            // when
            var elevationGain = undulatingStream.CalculateElevationGainForEffort(bestEffort);

            // then
            Assert.Equal(0, elevationGain);
        }

        [Fact]
        public void GivenAltitudeStreamWithNonIntegersWhenElevationGainIsCalculatedThenValuesAreNotRounded()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {0.0f, 10.4f, 0.1f, 10.5f, 0.9f, 10.6f, 0.5f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 0,
                EndIndex = 6
            };

            // when
            var elevationGain = undulatingStream.CalculateElevationGainForEffort(bestEffort);

            // then
            Assert.Equal(30.5f, elevationGain);
        }

        [Fact]
        public void GivenBestEffortOnlyCoversPartOfStreamWhenElevationGainIsCalculatedThenOnlyValuesInBestEffortAreUsed()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {0.0f, 10.4f, 0.1f, 10.5f, 0.9f, 10.6f, 0.5f, 20.3f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 2,
                EndIndex = 5
            };

            // when
            var elevationGain = undulatingStream.CalculateElevationGainForEffort(bestEffort);

            // then
            Assert.Equal(20.1f, elevationGain);
        }

        [Fact]
        public void GivenAltitudeStreamThatUndulatesWhenElevationLossIsCalculatedThenElevationGainIsIgnored()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {0.0f, 10.0f, 0.0f, 10.0f, 0.0f, 10.0f, 5.0f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 0,
                EndIndex = 6
            };

            // when
            var elevationLoss = undulatingStream.CalculateElevationLossForEffort(bestEffort);

            // then
            Assert.Equal(25, elevationLoss);
        }

        [Fact]
        public void GivenAltitudeStreamThatDescendsWhenElevationLossIsCalculatedThenEveryGapIsCounted()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {100.0f, 80.0f, 60.0f, 40.0f, 20.0f, 0.0f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 0,
                EndIndex = 6
            };

            // when
            var elevationLoss = undulatingStream.CalculateElevationLossForEffort(bestEffort);

            // then
            Assert.Equal(100, elevationLoss);
        }

        [Fact]
        public void GivenAltitudeStreamWithNonIntegersWhenElevationLossIsCalculatedThenValuesAreNotRounded()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {0.0f, 10.4f, 0.1f, 10.5f, 0.9f, 10.6f, 0.5f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 0,
                EndIndex = 6
            };

            // when
            var elevationLoss = undulatingStream.CalculateElevationLossForEffort(bestEffort);

            // then
            Assert.Equal(30.0f, elevationLoss);
        }

        [Fact]
        public void GivenBestEffortOnlyCoversPartOfStreamWhenElevationLossIsCalculatedThenOnlyValuesInBestEffortAreUsed()
        {
            // given
            var undulatingStream = new StravaAltitudeStream
            {
                Data = new[] {0.0f, 10.4f, 0.1f, 10.5f, 0.9f, 10.6f, 0.5f, 20.3f}
            };
            var bestEffort = new StravaBestEffort
            {
                StartIndex = 2,
                EndIndex = 5
            };

            // when
            var elevationLoss = undulatingStream.CalculateElevationLossForEffort(bestEffort);

            // then
            Assert.Equal(9.6f, elevationLoss);
        }
    }
}
