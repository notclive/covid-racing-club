﻿using System;
using Service.Models.Database;
using Service.Models.Static.Race;
using Xunit;

namespace src.Tests.Models.Database
{
    public class RaceTimeCalculationsTests
    {
        [Fact]
        public void WhenRaceIsOnSundayThenRaceResultsAreReleasedTuesdayMorningUtc()
        {
            // when
            var race = new Race
            {
                Date = new DateTime(2020, 4, 5)
            };

            // then
            Assert.Equal
            (
                new DateTime(2020, 4, 7, 11, 0, 0, DateTimeKind.Utc),
                race.GetTimeThatResultsAreReleased()
            );
        }

        [Fact]
        public void WhenRaceIsOnFridayThenRaceResultsAreReleasedSundayMorningUtc()
        {
            // when
            var race = new Race
            {
                Date = new DateTime(2020, 4, 3)
            };

            // then
            Assert.Equal
            (
                new DateTime(2020, 4, 5, 11, 0, 0, DateTimeKind.Utc),
                race.GetTimeThatResultsAreReleased()
            );
        }
    }
}
