aws dynamodb create-table \
    --table-name Race \
    --attribute-definitions AttributeName=RaceId,AttributeType=N \
    --key-schema AttributeName=RaceId,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "1"},
        "Name": {"S": "Before Calendar"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-03-29T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "2"},
        "Name": {"S": "Calendar Start"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-03-30T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "3"},
        "Name": {"S": "Last Sunday Race"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-04-05T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "4"},
        "Name": {"S": "Last Monday Race"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-04-06T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "5"},
        "Name": {"S": "Open Race"},
        "Description": {"S": "Motivational text is not my strong point."},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-04-10T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "6"},
        "Name": {"S": "Upcoming Race"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-04-15T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "7"},
        "Name": {"S": "Next Thursday Race"},
        "Description": {"S": "You will not want to miss this race."},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-04-16T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "8"},
        "Name": {"S": "Next Friday Race"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-04-17T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "9"},
        "Name": {"S": "Calendar End"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-05-03T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Race \
    --item '{
        "RaceId": {"N": "10"},
        "Name": {"S": "After Calendar"},
        "RaceDistance": {"S": "FiveKilometers"},
        "Date": {"S": "2020-05-04T00:00:00.000Z"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb create-table \
    --table-name Athlete \
    --attribute-definitions AttributeName=AthleteId,AttributeType=N \
    --key-schema AttributeName=AthleteId,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name Athlete \
    --item '{
        "AthleteId": {"N": "680358211"},
        "FirstName": {"S": "Fast"},
        "LastName": {"S": "Fred"},
        "EmailAddress": {"S": "fast-fred@example.com"},
        "Sex": {"S": "Male"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb create-table \
    --table-name RaceSubmission \
    --attribute-definitions AttributeName=RaceId,AttributeType=N AttributeName=AthleteId,AttributeType=N \
    --key-schema AttributeName=RaceId,KeyType=HASH AttributeName=AthleteId,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name RaceSubmission \
    --item '{
        "RaceId": {"N": "1"},
        "AthleteId": {"N": "680358211"},
        "ActivityId": {"N": "53209581112991"},
        "FirstName": {"S": "Fast"},
        "LastName": {"S": "Fred"},
        "RaceTimeInSeconds": {"N": "964"},
        "Sex": {"S": "Male"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name RaceSubmission \
    --item '{
        "RaceId": {"N": "1"},
        "AthleteId": {"N": "8246614455"},
        "ActivityId": {"N": "394727129953422"},
        "FirstName": {"S": "Quick"},
        "LastName": {"S": "Kate"},
        "RaceTimeInSeconds": {"N": "1304"},
        "Sex": {"S": "Female"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name RaceSubmission \
    --item '{
        "RaceId": {"N": "1"},
        "AthleteId": {"N": "546129421"},
        "ActivityId": {"N": "4309572828521744"},
        "FirstName": {"S": "Moderate"},
        "LastName": {"S": "Mike"},
        "RaceTimeInSeconds": {"N": "1329"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name RaceSubmission \
    --item '{
        "RaceId": {"N": "1"},
        "AthleteId": {"N": "23566812174"},
        "ActivityId": {"N": "67422134123682"},
        "FirstName": {"S": "Average"},
        "LastName": {"S": "Alice"},
        "RaceTimeInSeconds": {"N": "1505"},
        "Sex": {"S": "Female"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name RaceSubmission \
    --item '{
        "RaceId": {"N": "1"},
        "AthleteId": {"N": "933313662"},
        "ActivityId": {"N": "995581291455964"},
        "FirstName": {"S": "Slow"},
        "LastName": {"S": "Sam"},
        "RaceTimeInSeconds": {"N": "1852"},
        "Sex": {"S": "Male"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb put-item \
    --table-name RaceSubmission \
    --item '{
        "RaceId": {"N": "4"},
        "AthleteId": {"N": "680358211"},
        "ActivityId": {"N": "23128521345432"},
        "FirstName": {"S": "Fast"},
        "LastName": {"S": "Fred"},
        "RaceTimeInSeconds": {"N": "1002"},
        "Sex": {"S": "Male"}
      }' \
    --endpoint-url http://localhost:8000

aws dynamodb create-table \
    --table-name ResultsNotification \
    --attribute-definitions AttributeName=RaceId,AttributeType=N AttributeName=AthleteId,AttributeType=N \
    --key-schema AttributeName=RaceId,KeyType=HASH AttributeName=AthleteId,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000

aws dynamodb create-table \
    --table-name RaceReminder \
    --attribute-definitions AttributeName=RaceId,AttributeType=N AttributeName=AthleteId,AttributeType=N \
    --key-schema AttributeName=RaceId,KeyType=HASH AttributeName=AthleteId,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000
