resource "aws_route53_zone" "covid_racing_club" {
  name = "covidracing.club"

  tags = {
    project = "covid-racing-club"
  }
}

resource "aws_acm_certificate" "covid_racing_club" {

  domain_name               = "covidracing.club"
  validation_method         = "DNS"
  subject_alternative_names = ["*.covidracing.club"]

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    project = "covid-racing-club"
  }
}

resource "aws_route53_record" "certificate_validation" {
  name    = aws_acm_certificate.covid_racing_club.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.covid_racing_club.domain_validation_options.0.resource_record_type
  records = [aws_acm_certificate.covid_racing_club.domain_validation_options.0.resource_record_value]
  zone_id = aws_route53_zone.covid_racing_club.zone_id
  ttl     = 60
}

resource "aws_acm_certificate_validation" "covid_racing_club" {
  certificate_arn         = aws_acm_certificate.covid_racing_club.arn
  validation_record_fqdns = [aws_route53_record.certificate_validation.fqdn]
}

resource "aws_route53_record" "spf_and_google_search_console" {

  name    = "covidracing.club"
  zone_id = aws_route53_zone.covid_racing_club.zone_id
  type    = "TXT"
  ttl     = 300

  records = [
    "Sendinblue-code:c03c0e61e3da1f5bf22340e23848c135",
    "v=spf1 include:spf.sendinblue.com mx ~all",
    "google-site-verification=6vEJJpGjsfykYnhi9UquWbr1g7eN4mKSCak3Ry1rUXQ"
  ]
}

resource "aws_route53_record" "dmarc" {

  name    = "_dmarc.covidracing.club"
  zone_id = aws_route53_zone.covid_racing_club.zone_id
  type    = "TXT"
  ttl     = 300

  records = [
    "v=DMARC1; p=none; sp=none; rua=mailto:dmarc@mailinblue.com!10m; ruf=mailto:dmarc@mailinblue.com!10m; rf=afrf; pct=100; ri=86400"
  ]
}

resource "aws_route53_record" "domain_key" {

  name    = "mail._domainkey.covidracing.club."
  zone_id = aws_route53_zone.covid_racing_club.zone_id
  type    = "TXT"
  ttl     = 300

  records = [
    "k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeMVIzrCa3T14JsNY0IRv5/2V1/v2itlviLQBwXsa7shBD6TrBkswsFUToPyMRWC9tbR/5ey0nRBH0ZVxp+lsmTxid2Y2z+FApQ6ra2VsXfbJP3HE6wAO0YTVEJt1TmeczhEd2Jiz/fcabIISgXEdSpTYJhb0ct0VJRxcg4c8c7wIDAQAB"
  ]
}
