resource "aws_cloudwatch_event_rule" "every_15_minutes" {

    name = "covid-racing-club-every-15-minutes"
    schedule_expression = "rate(15 minutes)"

    tags = {
        project = "covid-racing-club"
    }
}

data "aws_lambda_function" "service" {
    function_name = "covid-racing-club-service"
}

resource "aws_cloudwatch_event_target" "send_due_results_notifications" {

    rule = aws_cloudwatch_event_rule.every_15_minutes.name
    arn = data.aws_lambda_function.service.arn

    input = jsonencode({
        "path" = "results-notifications/send-due"
        "httpMethod" = "POST"
    })
}

resource "aws_cloudwatch_event_target" "send_due_race_reminders" {

    rule = aws_cloudwatch_event_rule.every_15_minutes.name
    arn = data.aws_lambda_function.service.arn

    input = jsonencode({
        "path" = "race-reminders/send-due"
        "httpMethod" = "POST"
    })
}

resource "aws_route53_record" "service" {

  name    = "service.covidracing.club"
  zone_id = aws_route53_zone.covid_racing_club.zone_id
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.service.domain_name
    zone_id                = aws_cloudfront_distribution.service.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_cloudfront_distribution" "service" {

  enabled = true
  aliases = ["service.covidracing.club"]

  origin {

    domain_name = "g15tbzqa97.execute-api.us-east-1.amazonaws.com"
    origin_id   = "service-gateway"
    origin_path = "/production"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1.2"]
    }
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.covid_racing_club.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016"
  }

  default_cache_behavior {
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["HEAD", "GET"]
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = "service-gateway"

    forwarded_values {
      query_string = true
      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    project = "covid-racing-club"
  }
}
