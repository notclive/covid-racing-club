terraform {
  required_version = "0.12.24"
}

provider "aws" {
  version = "= 2.56.0"
  region  = "us-east-1"
}

resource "aws_s3_bucket" "terraform_state" {

  bucket = "covid-racing-club-terraform"
  acl    = "private"

  tags = {
    project = "covid-racing-club"
  }
}
