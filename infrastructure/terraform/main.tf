terraform {
  required_version = "0.12.24"

  # This bucket is created by the Terraform script in one-off/terraform-state-bucket/main.tf.
  backend "s3" {
    bucket   = "covid-racing-club-terraform"
    region   = "us-east-1"
    key      = "terraform.tfstate"
    encrypt  = "true"
  }
}

provider "aws" {
  version = "= 2.56.0"
  region  = "us-east-1"
}
