resource "aws_s3_bucket" "frontend" {

  bucket = "covidracing.club"
  acl    = "public-read"

  tags = {
    project = "covid-racing-club"
  }
}

data "aws_iam_policy_document" "frontend_public" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.frontend.arn}/*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

resource "aws_s3_bucket_policy" "frontend_public" {
  bucket = aws_s3_bucket.frontend.id
  policy = data.aws_iam_policy_document.frontend_public.json
}

resource "aws_route53_record" "frontend" {

  name    = "covidracing.club"
  zone_id = aws_route53_zone.covid_racing_club.zone_id
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.frontend.domain_name
    zone_id                = aws_cloudfront_distribution.frontend.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_cloudfront_distribution" "frontend" {

  enabled = true
  aliases = ["covidracing.club"]
  default_root_object = "index.html"

  origin {
    domain_name = aws_s3_bucket.frontend.bucket_domain_name
    origin_id   = aws_s3_bucket.frontend.id
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.covid_racing_club.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016"
  }

  default_cache_behavior {
    allowed_methods        = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods         = ["HEAD", "GET"]
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = aws_s3_bucket.frontend.id

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  # This prevents index.html from being cached, we don't want index.html to be cached so that releases are picked up instantly.
  # Other resources can be cached because their paths will change across releases, e.g. main.07c350af.chunk.js.
  ordered_cache_behavior {
    path_pattern           = "/index.html"
    allowed_methods        = ["HEAD", "GET"]
    cached_methods         = ["HEAD", "GET"]
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = aws_s3_bucket.frontend.id
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  custom_error_response {
    error_code         = "404"
    response_code      = "200"
    response_page_path = "/index.html"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    project = "covid-racing-club"
  }
}
