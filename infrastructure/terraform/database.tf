resource "aws_dynamodb_table" "race" {

  name           = "Race"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "RaceId"

  attribute {
    name = "RaceId"
    type = "N"
  }

  tags = {
    project = "covid-racing-club"
  }
}

resource "aws_dynamodb_table" "race_submission" {

  name           = "RaceSubmission"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "RaceId"
  range_key      = "AthleteId"

  attribute {
    name = "RaceId"
    type = "N"
  }

  attribute {
    name = "AthleteId"
    type = "N"
  }

  tags = {
    project = "covid-racing-club"
  }
}

resource "aws_dynamodb_table" "race_reminder" {

  name           = "RaceReminder"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "RaceId"
  range_key      = "AthleteId"

  attribute {
    name = "RaceId"
    type = "N"
  }

  attribute {
    name = "AthleteId"
    type = "N"
  }

  tags = {
    project = "covid-racing-club"
  }
}
