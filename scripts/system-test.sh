# Start test database
cd ../service/dummy-environment
docker-compose up

# Populate test database
./dynamodb/populate-database.sh

# Start service
cd ..
ASPNETCORE_ENVIRONMENT=Development dotnet run --project=src

# Start frontend
cd ../frontend
yarn start

# Run cypress tests
yarn run cypress run