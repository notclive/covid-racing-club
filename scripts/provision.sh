cd ../infrastructure/terraform
docker run --rm -it --volume ${HOME}/.aws:/root/.aws:ro --volume ${PWD}:/workspace --workdir /workspace hashicorp/terraform:0.12.24 init
docker run --rm -it --volume ${HOME}/.aws:/root/.aws:ro --volume ${PWD}:/workspace --workdir /workspace hashicorp/terraform:0.12.24 apply
