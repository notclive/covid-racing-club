cd ../service
dotnet publish --configuration Release
cd src/bin/Release/netcoreapp2.1/publish
zip -r - . > ../../../../../build/release.zip
cd ../../../../..
aws lambda update-function-code --region us-east-1 --function-name covid-racing-club-service --zip-file fileb://build/release.zip
