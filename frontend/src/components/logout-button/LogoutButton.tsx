import React, {useCallback, useContext} from 'react';
import {LoggedInAthleteContext} from '../../contexts/LoggedInAthleteContext';
import {Dropdown, DropdownButton} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';

const LogoutButton: React.FC = () => {

    const history = useHistory();
    const [loggedInAthlete, setLoggedInAthlete] = useContext(LoggedInAthleteContext);

    const logout = useCallback(() => {
        setLoggedInAthlete(undefined);
        history.push('/');
    }, [setLoggedInAthlete, history]);

    if (!loggedInAthlete) {
        return null;
    }

    return <DropdownButton id='logout-button'
                           variant='outline-primary'
                           title={`${loggedInAthlete.firstName} ${loggedInAthlete.lastName}`}
    >
        <Dropdown.Item onClick={logout}>Log out</Dropdown.Item>
    </DropdownButton>;
};

export default LogoutButton;
