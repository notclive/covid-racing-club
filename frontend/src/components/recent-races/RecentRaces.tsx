import React, {useMemo} from 'react';
import {Link} from 'react-router-dom';
import {DateTime} from 'luxon';
import {Race, sortByDate} from '../../models/Race';
import Card from '../reusable/card/Card';
import {DaySummary} from '../day-summary/DaySummary';

const YESTERDAY = DateTime.local().minus({days: 1});

export const RecentRaces: React.FC<Props> = ({races}) => {

    const oldestRace = useMemo(() => sortByDate(races)[0], [races]);

    const daysWithRecentRaces = useMemo(() => {
        let day = YESTERDAY;
        const days = [day];
        while (day > DateTime.fromISO(oldestRace?.date)) {
            day = day.minus({days: 1});
            days.push(day);
        }
        return days;
    }, [oldestRace]);

    return <>
        <h1>Recent Races</h1>
        <div className='my-3'>
            <Card>
                {daysWithRecentRaces.map(day => <DaySummary key={day.valueOf()} day={day} races={races}/>)}
            </Card>
        </div>
        <Link to='/race-calendar'>Find more results on the calendar</Link>
    </>;
};

interface Props {
    races: Race[];
}
