import React from 'react';
import {Link} from 'react-router-dom';
import {DateTime} from 'luxon';
import {Race} from '../../models/Race';
import Card from '../reusable/card/Card';
import {DaySummary} from '../day-summary/DaySummary';

const TODAY = DateTime.local();

export const UpcomingRaces: React.FC<Props> = ({races}) => {

    const daysOfWeek = [0, 1, 2, 3, 4, 5, 6].map(offset => TODAY.plus({days: offset}));

    return <>
        <h1 className='mb-3'>Upcoming Races</h1>
        <div className='my-3'>
            <Card>
                {daysOfWeek.map(day => <DaySummary key={day.valueOf()} day={day} races={races}/>)}
            </Card>
        </div>
        <Link to='/race-calendar'>Find more races on the calendar</Link>
    </>;
};

interface Props {
    races: Race[];
}

