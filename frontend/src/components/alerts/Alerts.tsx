import React from 'react';
import {Alert} from 'react-bootstrap';
import {useLocation} from 'react-router-dom';

export const Alerts: React.FC = () => {

    const {state} = useLocation<AlertsState>();

    if (state?.resultsNotificationRequested) {
        return <Alert variant='success'>
            ✉ You will be emailed when results are released.
        </Alert>;
    }

    if (state?.raceReminderRequested) {
        return <Alert variant='success'>
            ✉ You will be emailed on race day.
        </Alert>;
    }

    return null;
};

interface AlertsState {
    resultsNotificationRequested: boolean;
    raceReminderRequested: boolean;
}
