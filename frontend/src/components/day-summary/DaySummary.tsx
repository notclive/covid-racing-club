import React, {useCallback, useMemo} from 'react';
import {DateTime} from 'luxon';
import {RaceSummary} from '../race-summary/RaceSummary';
import {Race} from '../../models/Race';
import {isSameDayIgnoringTimeZone} from '../../common/dates/dates';

export const DaySummary: React.FC<Props> = ({day, races}) => {

    const isRaceOnDayIgnoringTimeZone = useCallback((race: Race) => {
        const raceDate = DateTime.fromISO(race.date);
        return isSameDayIgnoringTimeZone(day, raceDate);
    }, [day]);

    const matchingRace = useMemo(() => {
        return races.find(isRaceOnDayIgnoringTimeZone)
    }, [races, isRaceOnDayIgnoringTimeZone]);

    return <>
        <div className='card'>
            <div className='card-header'>
                <h6 className='m-0'>{day.weekdayLong}</h6>
            </div>
            <div className='card-body'>
                {matchingRace && <RaceSummary race={matchingRace}/>}
                {!matchingRace && <span className='text-muted'>No races</span>}
            </div>
        </div>
    </>;
};

interface Props {
    day: DateTime;
    races: Race[];
}
