import './HowItWorks.css';

import React from 'react';
import Card from '../reusable/card/Card';

function HowItWorks() {
    return <div className='my-4'>
        <Card>
            <div className='card-body'>
                <h1 className="display-4">Racing in isolation</h1>
                <p className="lead">
                    The Covid Racing Club is here to keep you motivated during the coronavirus pandemic.
                </p>
                <hr className="my-4"/>
                <h2>How it works</h2>
                <div>
                    <span className='emoji-bullet-point' role='img' aria-label='Step one'>📅</span>
                    Find a race that works for your training schedule.
                </div>
                <div>
                    <span className='emoji-bullet-point' role='img' aria-label='Step two'>🏃</span>
                    Run the race distance, on your own, on the day of the race.
                </div>
                <div>
                    <span className='emoji-bullet-point' role='img' aria-label='Step three'>🛰</span>
                    Submit the Strava recording of your run.
                </div>
                <div>
                    <span className='emoji-bullet-point' role='img' aria-label='Step four'>🏅</span>
                    Once entries are closed, see where you placed.
                </div>
            </div>
        </Card>
    </div>;
}

export default HowItWorks;
