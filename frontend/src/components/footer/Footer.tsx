import React from 'react';

const Footer: React.FC = () => {
    return <footer className='text-center my-3'>
        Made by <a href='mailto:notclive@gmail.com'>Jonathan Price</a>
    </footer>
};

export default Footer;
