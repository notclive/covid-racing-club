import React, {useState} from 'react';
import {DateTime} from 'luxon';
import HowItWorks from '../../how-it-works/HowItWorks';
import {UpcomingRaces} from '../../upcoming-races/UpcomingRaces';
import {RecentRaces} from '../../recent-races/RecentRaces';
import {Race} from '../../../models/Race';
import useRetryingRequest from '../../hooks/use-retrying-request/UseRetryingRequest';
import {handleJsonResponse} from '../../../common/network-response-handling/network-response-handling';
import LoadingSpinner from '../../reusable/loading-spinner/LoadingSpinner';

const FOUR_DAYS_AGO = DateTime.utc().minus({days: 4});
const ONE_WEEKS_TIME = DateTime.utc().plus({days: 8});

const HomePage: React.FC = () => {

    const [races, setRaces] = useState<Race[]>();

    const [requestIsInProgress, renderRetryMessage] = useRetryingRequest(() => {
        // Without Z suffix on date service interprets dates as local server time.
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races?after=${FOUR_DAYS_AGO.toISODate()}Z&before=${ONE_WEEKS_TIME.toISODate()}Z`)
            .then(handleJsonResponse)
            .then(setRaces);
    }, {makeRequestImmediately: true});

    return <>
        <HowItWorks/>
        {races && <div className='row'>
            <div className='col-md-6 my-4'>
                <RecentRaces races={races}/>
            </div>
            <div className='col-md-6 my-4'>
                <UpcomingRaces races={races}/>
            </div>
        </div>}
        {!races && requestIsInProgress && <LoadingSpinner/>}
        {!races && renderRetryMessage()}
    </>;
};

export default HomePage;
