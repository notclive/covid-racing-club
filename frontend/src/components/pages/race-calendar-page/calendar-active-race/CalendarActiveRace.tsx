import React from 'react';
import {Link} from 'react-router-dom';
import {Race} from '../../../../models/Race';

export const CalendarActiveRace: React.FC<Props> = ({race}) => {

    return <>
        {race.description && <div className='my-2'>{race.description}</div>}
        <div className='mt-2'>
            <Link className='btn btn-primary' to={`/races/${race.raceId}`}>Enter</Link>
        </div>
    </>;
};

interface Props {
    race: Race;
}
