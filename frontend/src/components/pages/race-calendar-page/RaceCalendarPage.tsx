import React from 'react';
import {Link, useParams} from 'react-router-dom';
import {DateTime} from 'luxon';
import CalendarMonth from './calendar-month/CalendarMonth';

const now = DateTime.local();

const RaceCalendarPage: React.FC = () => {

    const params = useParams<CalendarParams>();
    const year = Number.parseInt(params.year) || now.year;
    const month = Number.parseInt(params.month) || now.month;

    const calendarDate = DateTime.utc(year, month);
    const previousMonth = calendarDate.minus({months: 1});
    const nextMonth = calendarDate.plus({months: 1});

    return <>
        <h1 className='mb-3'>
            <Link to={`/race-calendar/${previousMonth.year}/${previousMonth.month}`} className='text-decoration-none'>⬅</Link>
            {/* Use short month so that next-link doesn't move around too much. */}
            <span className='mx-3'>{calendarDate.toLocaleString({month: 'short', year: 'numeric'})}</span>
            <Link to={`/race-calendar/${nextMonth.year}/${nextMonth.month}`} className='text-decoration-none'>➡</Link>
        </h1>
        <CalendarMonth year={year} month={month}/>
    </>;
};

interface CalendarParams {
    year: string;
    month: string;
}

export default RaceCalendarPage;
