import React from 'react';
import {
    determineWhetherRaceIsAcceptingSubmissions,
    determineWhetherResultsHaveBeenReleased,
    Race
} from '../../../../models/Race';
import {CalendarClosedRace} from '../calendar-closed-race/CalendarClosedRace';
import {CalendarActiveRace} from '../calendar-active-race/CalendarActiveRace';
import {CalendarUpcomingRace} from '../calendar-upcoming-race/CalendarUpcomingRace';

export const CalendarRace: React.FC<Props> = ({race}) => {

    if (determineWhetherResultsHaveBeenReleased(race)) {
        return <CalendarClosedRace race={race}/>;
    }

    if (determineWhetherRaceIsAcceptingSubmissions(race)) {
        return <CalendarActiveRace race={race}/>
    }

    return <CalendarUpcomingRace race={race}/>;
};

interface Props {
    race: Race;
}
