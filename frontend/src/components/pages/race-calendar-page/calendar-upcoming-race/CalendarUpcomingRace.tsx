import React from 'react';
import {Race} from '../../../../models/Race';
import {Link} from 'react-router-dom';

export const CalendarUpcomingRace: React.FC<Props> = ({race}) => {

    return <>
        {race.description && <div className='my-2'>{race.description}</div>}
        <div className='my-2'>
            <Link className='btn btn-primary' to={`?requestReminderForRace=${race.raceId}`}>
                Remind me
            </Link>
        </div>
    </>;
};

interface Props {
    race: Race;
}
