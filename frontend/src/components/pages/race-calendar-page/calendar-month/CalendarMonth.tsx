import styles from './CalendarMonth.module.css';
import React, {useEffect, useState} from 'react';
import {DateTime, Info} from 'luxon';
import {Race} from '../../../../models/Race';
import useRetryingRequest from '../../../hooks/use-retrying-request/UseRetryingRequest';
import LoadingSpinner from '../../../reusable/loading-spinner/LoadingSpinner';
import {handleJsonResponse} from '../../../../common/network-response-handling/network-response-handling';
import {CalendarRace} from '../calendar-race/CalendarRace';
import {isSameDayIgnoringTimeZone} from '../../../../common/dates/dates';

const TODAY = DateTime.local();

const CalendarMonth: React.FC<Props> = ({month, year}) => {

    const firstDayOfMonth = DateTime.utc(year, month);
    const lastDayOfMonth = firstDayOfMonth.endOf('month');
    const firstDayOfCalendar = firstDayOfMonth.startOf('week');
    const lastDayOfCalendar = lastDayOfMonth.endOf('week');
    const [races, setRaces] = useState<Race[]>();

    const [requestIsInProgress, renderRetryMessage, fetchRaces] = useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races?after=${firstDayOfCalendar.toISO()}&before=${lastDayOfCalendar.toISO()}`)
            .then(handleJsonResponse)
            .then(setRaces);
    });

    useEffect(() => {
        fetchRaces();
        // fetchRaces is not memoized so changes on each render.
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [month, year]);

    const renderWeekday = (weekday: string) => {
        return <div key={weekday} className='card'>
            <div className='card-body p-2'>
                <h5 className='card-text'>{weekday}</h5>
            </div>
        </div>
    };

    const renderWeekdays = () => {
        return <>
            <div className='card-group d-none d-sm-flex d-lg-none'>
                {Info.weekdays('short').map(renderWeekday)}
            </div>
            <div className='card-group d-none d-lg-flex'>
                {Info.weekdays('long').map(renderWeekday)}
            </div>
        </>;
    };

    const renderDay = (day: DateTime, races: Race[]) => {
        const isOutsideMonth = day.month !== firstDayOfMonth.month;
        const isToday = isSameDayIgnoringTimeZone(day, TODAY);
        const matchingRace = races.find(r => +DateTime.fromISO(r.date) === +day);
        return <div key={day.valueOf()} className={`card ${styles.calendarDay} ${isToday ? 'bg-light' : ''}`}>
            <div className='card-body p-2'>
                <div className='card-text'>
                    <div className='d-flex justify-content-between'>
                        <h6 className='m-0 font-weight-bold'>{matchingRace && matchingRace.name}</h6>
                        <h6 className={`ml-1 ${isOutsideMonth ? 'text-muted' : ''}`}>
                            {day.toLocaleString({day: 'numeric'})}
                        </h6>
                    </div>
                    {matchingRace && <CalendarRace race={matchingRace}/>}
                </div>
            </div>
        </div>
    };

    const renderWeek = (firstDayOfWeek: DateTime, races: Race[]) => {
        const days = [];
        for (let i = 0; i < 7; i++) {
            days.push(renderDay(firstDayOfWeek.plus({days: i}), races));
        }
        return <div key={firstDayOfWeek.valueOf()} className='card-group'>
            {days}
        </div>;
    };

    const renderCalendar = (races: Race[]) => {
        let firstDayOfWeek = firstDayOfCalendar;
        const weeks = [];
        while (firstDayOfWeek <= lastDayOfMonth) {
            weeks.push(renderWeek(firstDayOfWeek, races));
            firstDayOfWeek = firstDayOfWeek.plus({weeks: 1});
        }
        return <>
            {renderWeekdays()}
            {weeks}
        </>;
    };

    return <>
        {races && renderCalendar(races)}
        {!races && requestIsInProgress && <LoadingSpinner/>}
        {!races && renderRetryMessage()}
    </>;
};

interface Props {
    month: number;
    year: number;
}

export default CalendarMonth;
