import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {convertSubmissionsToResults, Result} from '../../../../models/Result';
import useRetryingRequest from '../../../hooks/use-retrying-request/UseRetryingRequest';
import {Submission} from '../../../../models/Submission';
import {handleJsonResponse} from '../../../../common/network-response-handling/network-response-handling';
import {Race} from '../../../../models/Race';

export const CalendarClosedRace: React.FC<Props> = ({race}) => {

    const [results, setResults] = useState<Result[]>();

    useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${race.raceId}/submissions`)
            .then<Submission[]>(handleJsonResponse)
            .then(convertSubmissionsToResults)
            .then(setResults);
    }, {makeRequestImmediately: true});

    const firstPlace = results && results[0];

    return <>
        {
            firstPlace && <div className='my-2'>
                <span className='mr-2' role='img' aria-label='Winner'>🏆</span>
                <a href={`https://strava.com/activities/${firstPlace.activityId}`}
                   target='_blank'
                   rel='noopener noreferrer'
                >
                    {firstPlace.firstName} {firstPlace.lastName}
                </a>
            </div>
        }
        <div className='mt-2'>
            <Link to={`/races/${race.raceId}`}>Results</Link>
        </div>
    </>;
};

interface Props {
    race: Race;
}
