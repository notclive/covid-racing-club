import React, {useState} from 'react';
import {Race} from '../../../../models/Race';
import {formatRaceTimeForDisplay, Submission} from '../../../../models/Submission';
import {convertSubmissionsToResults, Result} from '../../../../models/Result';
import useRetryingRequest from '../../../hooks/use-retrying-request/UseRetryingRequest';
import {handleJsonResponse} from '../../../../common/network-response-handling/network-response-handling';
import YourSubmission from '../your-submission/YourSubmission';
import LoadingSpinner from '../../../reusable/loading-spinner/LoadingSpinner';

const ClosedRace: React.FC<Props> = ({race}) => {

    const [results, setResults] = useState<Result[]>();

    const [requestIsInProgress, renderRetryMessage] = useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${race.raceId}/submissions`)
            .then<Submission[]>(handleJsonResponse)
            .then(convertSubmissionsToResults)
            .then(setResults);
    }, {makeRequestImmediately: true});

    const numberOfMaleResults = results?.filter(s => s.sex === 'Male').length;
    const numberOfFemaleResults = results?.filter(s => s.sex === 'Female').length;

    const renderResult = (result: Result) => {
        return <tr key={result.athleteId}>
            <th scope='row'>
                <span className='mr-4'>{result.overallPosition}</span>
                {result.malePosition && <div className='font-weight-normal d-sm-inline'>Male {result.malePosition} of {numberOfMaleResults}</div>}
                {result.femalePosition && <div className='font-weight-normal d-sm-inline'>Female {result.femalePosition} of {numberOfFemaleResults}</div>}
            </th>
            <td>
                <a href={`https://strava.com/athletes/${result.athleteId}`}
                   target='_blank'
                   rel='noopener noreferrer'
                >
                    {result.firstName} {result.lastName}
                </a>
            </td>
            <td>
                <a href={`https://strava.com/activities/${result.activityId}`}
                   target='_blank'
                   rel='noopener noreferrer'
                >
                    {formatRaceTimeForDisplay(result)}
                </a>
            </td>
        </tr>;
    };

    const renderResults = (results: Result[]) => {
        return <table className='table bg-white mt-4'>
            <thead>
            <tr>
                <th scope='col'>Position</th>
                <th scope='col'>Athlete</th>
                <th scope='col'>Time</th>
            </tr>
            </thead>
            <tbody>
            {results.map(renderResult)}
            </tbody>
        </table>
    };

    return <>
        {results && <YourSubmission submissions={results}/>}
        <h2>Results</h2>
        {results && renderResults(results)}
        {!results && requestIsInProgress && <LoadingSpinner/>}
        {!results && renderRetryMessage()}
    </>;
};

interface Props {
    race: Race;
}

export default ClosedRace;
