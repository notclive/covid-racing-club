import React, {useContext, useMemo} from 'react';
import {formatRaceTimeForDisplay, Submission} from '../../../../models/Submission';
import {LoggedInAthleteContext} from '../../../../contexts/LoggedInAthleteContext';

const YourSubmission: React.FC<Props> = ({submissions}) => {

    const [loggedInAthlete] = useContext(LoggedInAthleteContext);

    const loggedInAthletesSubmission = useMemo(() => {
        if (submissions && loggedInAthlete) {
            return submissions.find(s => s.athleteId === loggedInAthlete.athleteId);
        }
    }, [submissions, loggedInAthlete]);

    if (!loggedInAthletesSubmission) {
        return null;
    }

    return <>
        <h2>Your submission</h2>
        <p>
            You entered this race with a time of
            {' '}
            <a href={`https://strava.com/activities/${loggedInAthletesSubmission.activityId}`}>
                {formatRaceTimeForDisplay(loggedInAthletesSubmission)}
            </a>.
        </p>
    </>;
};

interface Props {
    submissions?: Submission[];
}

export default YourSubmission;
