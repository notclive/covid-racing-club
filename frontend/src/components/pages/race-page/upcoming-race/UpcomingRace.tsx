import React, {useMemo} from 'react';
import {DateTime} from 'luxon';
import {Race} from '../../../../models/Race';
import {formatFutureDateForDisplay} from '../../../../common/dates/dates';

const UpcomingRace: React.FC<Props> = ({race}) => {

    const whenRaceCanBeRun = useMemo(() => {
        return formatFutureDateForDisplay(DateTime.fromISO(race.earliestTimeRaceCanBeRun));
    }, [race.earliestTimeRaceCanBeRun]);

    return <>
        You can run this race {whenRaceCanBeRun}.<br/>
    </>;
};

interface Props {
    race: Race;
}

export default UpcomingRace;
