import React, {useMemo, useState} from 'react';
import {Link} from 'react-router-dom';
import useRaceIdFromPath from '../../hooks/use-race-id-from-path/UseRaceIdFromPath';
import {OpenRace} from './open-race/OpenRace';
import ClosedRace from './closed-race/ClosedRace';
import UpcomingRace from './upcoming-race/UpcomingRace';
import {handleJsonResponse} from '../../../common/network-response-handling/network-response-handling';
import LoadingSpinner from '../../reusable/loading-spinner/LoadingSpinner';
import useRetryingRequest from '../../hooks/use-retrying-request/UseRetryingRequest';
import {
    determineWhetherRaceIsAcceptingSubmissions,
    determineWhetherRaceIsUpcoming,
    determineWhetherResultsHaveBeenReleased,
    formatRaceDateForDisplay,
    Race
} from '../../../models/Race';

const RacePage: React.FC = () => {

    const raceId = useRaceIdFromPath();
    const [race, setRace] = useState<Race>();

    const [requestIsInProgress, renderRetryMessage] = useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${raceId}`)
            .then(handleJsonResponse)
            .then(setRace);
    }, {makeRequestImmediately: true});

    const resultsHaveBeenReleased = useMemo(() => race && determineWhetherResultsHaveBeenReleased(race), [race]);
    const raceIsAcceptingSubmissions = useMemo(() => race && determineWhetherRaceIsAcceptingSubmissions(race), [race]);
    const raceIsUpcoming = useMemo(() => race && determineWhetherRaceIsUpcoming(race), [race]);

    return <>
        {!race && requestIsInProgress && <LoadingSpinner/>}
        {!race && renderRetryMessage()}
        {race && <h1>{race.name} - {formatRaceDateForDisplay(race)}</h1>}
        {race && resultsHaveBeenReleased && <ClosedRace race={race}/>}
        {race && raceIsAcceptingSubmissions && <OpenRace race={race}/>}
        {race && raceIsUpcoming && <UpcomingRace race={race}/>}
        <Link to='/'>Find another race</Link>
    </>;
};

export default RacePage;
