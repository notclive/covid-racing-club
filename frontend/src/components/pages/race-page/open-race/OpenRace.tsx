import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {DateTime} from 'luxon';
import {Race} from '../../../../models/Race';
import {formatFutureDateForDisplay} from '../../../../common/dates/dates';
import YourSubmission from '../your-submission/YourSubmission';
import {Submission} from '../../../../models/Submission';
import {handleJsonResponse} from '../../../../common/network-response-handling/network-response-handling';
import YourStravaActivities from './your-strava-activities/YourStravaActivities';
import {Private} from '../../../reusable/private/Private';

export const OpenRace: React.FC<Props> = ({race}) => {

    const [submissions, setSubmissions] = useState<Submission[]>();

    useEffect(() => {
        // Prevent a cached response from being served, so that the user always sees their submission.
        fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${race.raceId}/submissions?cacheBuster=${DateTime.utc().valueOf()}`)
            .then(handleJsonResponse)
            .then(setSubmissions);
    }, [race.raceId, setSubmissions]);

    const renderResultsWillBeReleasedMessage = () => {
        return <>
            <p>
                Results will be released {formatFutureDateForDisplay(DateTime.fromISO(race.timeThatResultsAreReleased))},
                check back later.
            </p>
            <p>
                <Link to={`?requestResultsNotificationForRace=${race.raceId}`} className='btn btn-primary'>
                    Email me when results are released
                </Link>
            </p>
        </>
    };

    return <>
        <YourSubmission submissions={submissions}/>
        <Private>
            {
                loggedInAthlete => <YourStravaActivities race={race} loggedInAthlete={loggedInAthlete}/>
            }
        </Private>
        <h2>Results</h2>
        {renderResultsWillBeReleasedMessage()}
    </>;
};

interface Props {
    race: Race;
}

