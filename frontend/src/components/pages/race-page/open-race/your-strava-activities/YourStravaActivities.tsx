import React, {useState} from 'react';
import {Alert} from 'react-bootstrap';
import useRetryingRequest from '../../../../hooks/use-retrying-request/UseRetryingRequest';
import {Athlete} from '../../../../../contexts/LoggedInAthleteContext';
import {Race} from '../../../../../models/Race';
import {PotentialSubmission} from '../../../../../models/PotentialSubmission';
import PotentialSubmissionCard from './potential-submission-card/PotentialSubmissionCard';
import {handleJsonResponse} from '../../../../../common/network-response-handling/network-response-handling';
import LoadingSpinner from '../../../../reusable/loading-spinner/LoadingSpinner';

const YourStravaActivities: React.FC<Props> = ({race, loggedInAthlete}) => {

    const [potentialSubmissions, setPotentialSubmissions] = useState<PotentialSubmission[]>();
    const [submissionPostedSuccessfully, setSubmissionPostedSuccessfully] = useState<boolean>();

    const [requestIsInProgress, renderRetryMessage] = useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/athletes/${loggedInAthlete.athleteId}/potential-submissions?raceId=${race.raceId}&token=${loggedInAthlete.accessToken}`)
            .then(handleJsonResponse)
            .then(setPotentialSubmissions)
    }, {makeRequestImmediately: true});

    const renderSubmissionPostedSuccessfullyMessage = () => {
        return <Alert variant='success'>
            <Alert.Heading>Good job!</Alert.Heading>
            Your activity was submitted successfully.
        </Alert>;
    };

    const renderNoPotentialSubmissionsMessage = () => {
        return <Alert variant='warning' className='my-4'>
            <Alert.Heading>Go for a run!</Alert.Heading>
            You don't have any eligible Strava activities.<br/>
            Activities must be public.
        </Alert>;
    };

    return <>
        <h2>Your Strava Activities</h2>
        {submissionPostedSuccessfully && renderSubmissionPostedSuccessfullyMessage()}
        {potentialSubmissions && potentialSubmissions.length === 0 && renderNoPotentialSubmissionsMessage()}
        {potentialSubmissions && potentialSubmissions.map(
            ps => <PotentialSubmissionCard key={ps.activityId}
                                           race={race}
                                           loggedInAthlete={loggedInAthlete}
                                           submission={ps}
                                           submissionPostedSuccessfully={() => setSubmissionPostedSuccessfully(true)}
            />
        )}
        {!potentialSubmissions && requestIsInProgress && <LoadingSpinner/>}
        {!potentialSubmissions && renderRetryMessage()}
    </>
};

interface Props {
    race: Race;
    loggedInAthlete: Athlete;
}

export default YourStravaActivities;
