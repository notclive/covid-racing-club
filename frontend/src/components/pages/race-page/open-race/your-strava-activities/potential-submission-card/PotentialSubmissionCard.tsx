import React from 'react';
import {Button, Spinner} from 'react-bootstrap';
import useRetryingRequest from '../../../../../hooks/use-retrying-request/UseRetryingRequest';
import {verifyResponseSuccessful} from '../../../../../../common/network-response-handling/network-response-handling';
import Card from '../../../../../reusable/card/Card';
import {Race} from '../../../../../../models/Race';
import {Athlete} from '../../../../../../contexts/LoggedInAthleteContext';
import {
    formatActivityTimeForDisplay,
    formatRaceTimeForDisplay,
    PotentialSubmission
} from '../../../../../../models/PotentialSubmission';

const PotentialSubmissionCard: React.FC<Props> = ({race, submission, loggedInAthlete, submissionPostedSuccessfully}) => {

    const [requestIsInProgress, renderRetryMessage, postSubmission] = useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${race.raceId}/submissions?token=${loggedInAthlete.accessToken}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                activityId: submission.activityId
            })
        })
            .then(verifyResponseSuccessful)
            .then(submissionPostedSuccessfully);
    });

    return <div className='my-4'>
        <Card disabled={submission.eligibility !== 'Eligible'}>
            <div className='card-header'>
                <h3>{submission.name}</h3>
                <h5>{formatActivityTimeForDisplay(submission)}</h5>
            </div>
            <div className='card-body'>
                <div className='card-text'>
                    {
                        submission.eligibility === 'Eligible' && <>
                            <p>
                                <b>Race time:</b> {formatRaceTimeForDisplay(submission)}
                            </p>
                            {renderRetryMessage()}
                            <Button onClick={postSubmission}
                                    className='d-flex align-items-center'
                                    disabled={requestIsInProgress}
                            >
                                Submit this activity
                                {
                                    requestIsInProgress && <Spinner animation='border' className='ml-1' size='sm'/>
                                }
                            </Button>
                        </>
                    }
                    {
                        submission.eligibility === 'NotRun' && <>This activity is ineligible, it's not a run.</>
                    }
                    {
                        submission.eligibility === 'TooShort' && <>This activity is ineligible, it's too short.</>
                    }
                </div>
            </div>
        </Card>
    </div>
};

interface Props {
    race: Race;
    submission: PotentialSubmission;
    loggedInAthlete: Athlete;
    submissionPostedSuccessfully: () => any;
}

export default PotentialSubmissionCard;
