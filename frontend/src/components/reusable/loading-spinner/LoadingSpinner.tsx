import React from 'react';
import {Spinner} from 'react-bootstrap';

const LoadingSpinner: React.FC = () => {
    return <div className='my-5 text-center'>
        <Spinner animation='border'/>
    </div>;
};

export default LoadingSpinner;
