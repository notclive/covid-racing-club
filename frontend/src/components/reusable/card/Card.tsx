import React from 'react';

const Card: React.FC<Props> = ({disabled = false, children}) => {
    return <div className={'border-2 border-bottom border-primary rounded shadow ' + (disabled ? 'bg-grey' : 'bg-white')}>
        {children}
    </div>;
};

interface Props {
    disabled?: boolean;
}

export default Card;
