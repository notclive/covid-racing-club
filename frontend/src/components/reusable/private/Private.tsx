import React, {useContext, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {Athlete, LoggedInAthleteContext} from '../../../contexts/LoggedInAthleteContext';
import {useLocationBeforeLogin} from '../../hooks/use-location-before-login/UseLocationBeforeLogin';
import LoadingSpinner from '../loading-spinner/LoadingSpinner';

export const Private: React.FC<Props> = ({children}) => {

    const history = useHistory();
    const [saveLocation] = useLocationBeforeLogin();
    const [loggedInAthlete] = useContext(LoggedInAthleteContext);

    useEffect(() => {
        if (!loggedInAthlete) {
            saveLocation();
            history.push('/login/out');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loggedInAthlete]);

    return loggedInAthlete
        ? children(loggedInAthlete)
        : <LoadingSpinner/>;
};

interface Props {
    children: (athlete: Athlete) => JSX.Element;
}
