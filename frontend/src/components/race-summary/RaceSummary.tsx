import React from 'react';
import {
    determineWhetherRaceIsAcceptingSubmissions,
    determineWhetherResultsHaveBeenReleased,
    Race
} from '../../models/Race';
import {ClosedRaceSummary} from './closed-race-summary/ClosedRaceSummary';
import {UpcomingRaceSummary} from './upcoming-race-summary/UpcomingRaceSummary';
import {ActiveRaceSummary} from './active-race-summary/ActiveRaceSummary';

export const RaceSummary: React.FC<Props> = ({race}) => {

    if (determineWhetherResultsHaveBeenReleased(race)) {
        return <ClosedRaceSummary race={race}/>;
    }
    if (determineWhetherRaceIsAcceptingSubmissions(race)) {
        return <ActiveRaceSummary race={race}/>;
    }
    return <UpcomingRaceSummary race={race}/>;
};

interface Props {
    race: Race;
}
