import React from 'react';
import {Link} from 'react-router-dom';
import {Race} from '../../../models/Race';

export const ActiveRaceSummary: React.FC<Props> = ({race}) => {

    return <>
        <h4>{race.name}</h4>
        {race.description && <div className='my-3'>{race.description}</div>}
        <div className='my-3'>
            <Link className='btn btn-primary' to={`/races/${race.raceId}`}>Submit my Strava activity</Link>
        </div>
        <Link className='btn btn-primary' to={`?requestResultsNotificationForRace=${race.raceId}`}>
            Email me when results are released
        </Link>
    </>;
};

interface Props {
    race: Race;
}
