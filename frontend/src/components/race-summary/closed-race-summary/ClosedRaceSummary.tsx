import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {Race} from '../../../models/Race';
import useRetryingRequest from '../../hooks/use-retrying-request/UseRetryingRequest';
import {handleJsonResponse} from '../../../common/network-response-handling/network-response-handling';
import {convertSubmissionsToResults, Result} from '../../../models/Result';
import {formatRaceTimeForDisplay, Submission} from '../../../models/Submission';

export const ClosedRaceSummary: React.FC<Props> = ({race}) => {

    const [results, setResults] = useState<Result[]>();

    useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${race.raceId}/submissions`)
            .then<Submission[]>(handleJsonResponse)
            .then(convertSubmissionsToResults)
            .then(setResults);
    }, {makeRequestImmediately: true});

    const firstPlace = results && results[0];

    return <>
        <h4>{race.name}</h4>
        {firstPlace && <div className='my-3'>
            <span role='img' aria-label='Winner'>🏆</span>
            <a className='mx-2'
               href={`https://strava.com/athletes/${firstPlace.athleteId}`}
               target='_blank'
               rel='noopener noreferrer'
            >
                {firstPlace.firstName} {firstPlace.lastName}
            </a>
            <a href={`https://strava.com/activities/${firstPlace.activityId}`}
               target='_blank'
               rel='noopener noreferrer'
            >
                {formatRaceTimeForDisplay(firstPlace)}
            </a>
        </div>}
        <Link to={`/races/${race.raceId}`}>Results</Link>
    </>;
};

interface Props {
    race: Race;
}
