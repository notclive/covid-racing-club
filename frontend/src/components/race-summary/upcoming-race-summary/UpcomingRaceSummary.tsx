import React from 'react';
import {Race} from '../../../models/Race';
import {Link} from 'react-router-dom';

export const UpcomingRaceSummary: React.FC<Props> = ({race}) => {

    return <>
        <h4>{race.name}</h4>
        {race.description && <div className='my-3'>{race.description}</div>}
        <div className='mt-3'>
            <Link className='btn btn-primary' to={`?requestReminderForRace=${race.raceId}`}>
                Remind me about this race
            </Link>
        </div>
    </>;
};

interface Props {
    race: Race;
}
