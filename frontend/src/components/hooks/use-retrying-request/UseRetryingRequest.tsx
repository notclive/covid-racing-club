import React, {useEffect, useState} from 'react';
import {Alert, Button} from 'react-bootstrap';
import useInterval from '@rooks/use-interval';

const useRetryingRequest: (networkRequest: () => Promise<any>, options?: Options) => [boolean, RenderFunction, () => void] = (networkRequest, options) => {

    const [requestIsInProgress, setRequestIsInProgress] = useState<boolean>(false);
    const [requestFailed, setRequestFailed] = useState<boolean>(false);
    const [secondsUntilRetry, setSecondsUntilRetry] = useState<number>();

    const attemptNetworkRequest = () => {
        setRequestIsInProgress(true);
        networkRequest()
            .then(() => {
                setRequestFailed(false);
            })
            .catch(() => {
                setRequestFailed(true);
                setSecondsUntilRetry(10);
            })
            .finally(() => {
                setRequestIsInProgress(false);
            });
    };

    useInterval(() => {
        if (secondsUntilRetry === undefined) {
            return;
        }
        if (secondsUntilRetry <= 1) {
            setSecondsUntilRetry(undefined);
            attemptNetworkRequest();
            return;
        }
        setSecondsUntilRetry(secondsUntilRetry - 1);
    }, 1000, true);

    useEffect(() => {
        if (options?.makeRequestImmediately) {
            attemptNetworkRequest();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const renderRetryMessage = () => {
        if (!requestFailed) {
            return null;
        }
        return <Alert variant='danger' className='w-100'>
            {
                requestIsInProgress && <>Something went wrong, trying again.</>
            }
            {
                !requestIsInProgress && <>
                    <p>Something went wrong, trying again in {secondsUntilRetry} seconds.</p>
                    <Button onClick={attemptNetworkRequest}>Try again now</Button>
                </>
            }
        </Alert>
    };

    return [requestIsInProgress, renderRetryMessage, attemptNetworkRequest]
};

type RenderFunction = () => (null | JSX.Element);

interface Options {
    makeRequestImmediately: boolean;
}

export default useRetryingRequest;
