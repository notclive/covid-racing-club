import {useHistory} from 'react-router-dom';
import useSessionStorage from '@rooks/use-sessionstorage';

export const useLocationBeforeLogin = () => {

    const history = useHistory();
    const [locationBeforeLogin, setLocationBeforeLogin, deleteLocationBeforeLogin] = useSessionStorage('location-before-login');

    const saveLocation = () => {
        setLocationBeforeLogin(history.location);
    };

    const restoreLocation = () => {
        deleteLocationBeforeLogin();
        history.push(locationBeforeLogin || '/');
    };

    return [saveLocation, restoreLocation];
};
