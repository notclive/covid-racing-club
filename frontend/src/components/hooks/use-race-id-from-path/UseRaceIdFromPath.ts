import {useParams} from 'react-router-dom';

const useRaceIdFromPath = () => {
    const params = useParams<PathHasRaceId>();
    return parseInt(params.raceId);
};

interface PathHasRaceId {
    raceId: string;
}

export default useRaceIdFromPath;
