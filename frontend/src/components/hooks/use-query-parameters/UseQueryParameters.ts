import {useLocation} from 'react-router-dom';

export const useQueryParameters = () => {
    return new URLSearchParams(useLocation().search);
};
