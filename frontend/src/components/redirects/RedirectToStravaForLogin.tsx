import React from 'react';
import LoadingSpinner from '../reusable/loading-spinner/LoadingSpinner';

export const RedirectToStravaForLogin: React.FC = () => {

    const redirectUri = `${process.env.REACT_APP_FRONTEND_URL}/login/in`;
    window.location.assign(`https://www.strava.com/oauth/authorize?client_id=44831&scope=read,activity:read&response_type=code&redirect_uri=${redirectUri}`);

    return <LoadingSpinner/>;
};
