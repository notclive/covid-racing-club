import React, {Dispatch, SetStateAction, useCallback, useContext, useEffect, useMemo, useState} from 'react';
import {Alert, Button} from 'react-bootstrap';
import LoadingSpinner from '../reusable/loading-spinner/LoadingSpinner';
import {Athlete, LoggedInAthleteContext} from '../../contexts/LoggedInAthleteContext';
import {useQueryParameters} from '../hooks/use-query-parameters/UseQueryParameters';
import {useLocationBeforeLogin} from '../hooks/use-location-before-login/UseLocationBeforeLogin';

export const RedirectToPageAfterLogin: React.FC = () => {

    const queryParameters = useQueryParameters();
    const [failedToLogin, setFailedToLogin] = useState<boolean>(false);
    const restoreLocationBeforeLogin = useLocationBeforeLogin()[1];
    const setLoggedInAthlete = useContext<[Athlete | undefined, Dispatch<SetStateAction<Athlete | undefined>>]>(
        LoggedInAthleteContext
    )[1];

    const oauthCode = useMemo(() => queryParameters.get('code'), [queryParameters]);

    const fetchLoggedInAthlete = useCallback(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/athletes/login?code=${oauthCode}`, {
            method: 'POST'
        }).then(response => response.json())
    }, [oauthCode]);

    useEffect(() => {
        fetchLoggedInAthlete()
            .then(setLoggedInAthlete)
            .then(restoreLocationBeforeLogin)
            .catch(() => setFailedToLogin(true));
        // fetchLoggedInAthlete should only ever be called once.
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (failedToLogin) {
        return <Alert variant='danger'>
            <Alert.Heading>Oh no!</Alert.Heading>
            <p className='mb-2'>
                Something went wrong while logging in with Strava.
            </p>
            <Button variant='danger' onClick={restoreLocationBeforeLogin}>Try again</Button>
        </Alert>
    }

    return <LoadingSpinner/>;
};
