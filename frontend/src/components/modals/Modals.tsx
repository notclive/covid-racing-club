import React from 'react';
import {Private} from '../reusable/private/Private';
import {useQueryParameters} from '../hooks/use-query-parameters/UseQueryParameters';
import {ResultsNotificationModal} from './results-notification-modal/ResultsNotificationModal';
import {RaceReminderModal} from './race-reminder-modal/RaceReminderModal';

export const Modals: React.FC = () => {

    // Query parameters are used rather than a context so that they are restored after login redirects.
    const queryParameters = useQueryParameters();

    const safelyParseNumericQueryParameter = (parameter: string) => {
        const value = queryParameters.get(parameter);
        return value ? Number.parseInt(value) : undefined;
    };

    const resultsNotificationRaceId = safelyParseNumericQueryParameter('requestResultsNotificationForRace');
    if (resultsNotificationRaceId) {
        return <Private>
            {
                loggedInAthlete => <ResultsNotificationModal raceId={resultsNotificationRaceId}
                                                             loggedInAthlete={loggedInAthlete}/>
            }
        </Private>
    }

    const reminderRaceId = safelyParseNumericQueryParameter('requestReminderForRace');
    if (reminderRaceId) {
        return <Private>
            {
                loggedInAthlete => <RaceReminderModal raceId={reminderRaceId}
                                                      loggedInAthlete={loggedInAthlete}/>
            }
        </Private>
    }

    return null;
};
