import React, {FormEvent, useCallback, useState} from 'react';
import {Button, Form, Modal, Spinner} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';
import useRetryingRequest from '../../hooks/use-retrying-request/UseRetryingRequest';
import {verifyResponseSuccessful} from '../../../common/network-response-handling/network-response-handling';
import {Athlete} from '../../../contexts/LoggedInAthleteContext';

export const ResultsNotificationModal: React.FC<Props> = ({raceId, loggedInAthlete}) => {

    const history = useHistory();
    const [emailAddress, setEmailAddress] = useState(loggedInAthlete.emailAddress || '');
    const [formHasBeenValidated, setFormHasBeenValidated] = useState(false);

    const handleEmailAddressChange = useCallback((event: FormEvent<HTMLInputElement>) => {
        setEmailAddress(event.currentTarget.value);
    }, [setEmailAddress]);

    const closeModal = (resultsNotificationRequested: boolean) => {
        const pathWithoutQueryParameters = history.location.pathname;
        history.push(pathWithoutQueryParameters, {resultsNotificationRequested});
    };

    const [requestIsInProgress, renderRetryMessage, createNotification] = useRetryingRequest(() => {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/races/${raceId}/results-notifications?token=${loggedInAthlete.accessToken}`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({emailAddress})
        })
            .then(verifyResponseSuccessful)
            .then(() => closeModal(true));
    });

    const handleSubmit = useCallback((event: FormEvent<HTMLFormElement>) => {
        const form = event.currentTarget;
        event.preventDefault();
        event.stopPropagation();
        setFormHasBeenValidated(true);

        if (form.checkValidity()) {
            createNotification();
        }
    }, [setFormHasBeenValidated, createNotification]);

    return <Modal centered
                  show={true}
                  onHide={() => closeModal(false)}
                  aria-labelledby='results-notification-modal-title'
    >
        <Form noValidate validated={formHasBeenValidated} onSubmit={handleSubmit}>
            <Modal.Header closeButton>
                <Modal.Title id='results-notification-modal-title'>
                    How would you like to be notified?
                </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group controlId='email-address'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type='email'
                                  placeholder='athlete@domain.com'
                                  value={emailAddress}
                                  onChange={handleEmailAddressChange}
                                  required
                    />
                    <Form.Control.Feedback type='invalid'>
                        Please enter an email address
                    </Form.Control.Feedback>
                </Form.Group>
            </Modal.Body>

            <Modal.Footer>
                {renderRetryMessage()}
                <Button type='submit'
                        className='d-flex align-items-center'
                        disabled={requestIsInProgress}
                >
                    Email me
                    {
                        requestIsInProgress &&
                        <Spinner animation='border' className='ml-1' size='sm'/>
                    }
                </Button>
            </Modal.Footer>
        </Form>
    </Modal>;
};

interface Props {
    raceId: number;
    loggedInAthlete: Athlete;
}
