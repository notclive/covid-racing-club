import './Navbar.css';
import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import LogoutButton from '../logout-button/LogoutButton';

const Navbar: React.FC = () => {

    return <nav className='navbar navbar-light navbar-expand-sm bg-warning border-2 border-bottom border-primary shadow'>
        <div className='container flex-column align-items-center align-items-sm-start text-center text-sm-left'>
            <Link className='navbar-brand' to={'/'}>Covid Racing Club</Link>
            <ul className='navbar-nav align-self-stretch'>
                <li className='nav-item'>
                    <NavLink className='nav-link' activeClassName='active' exact={true} to={'/'}>
                        Current Races
                    </NavLink>
                </li>
                <li className='nav-item'>
                    <NavLink className='nav-link' activeClassName='active' exact={true} to={'/race-calendar'}>
                        All Races
                    </NavLink>
                </li>
                <li className='nav-item ml-sm-auto'>
                    <LogoutButton/>
                </li>
            </ul>
        </div>
    </nav>
};

export default Navbar;
