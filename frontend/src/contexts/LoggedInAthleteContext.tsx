import React, {createContext, Dispatch, SetStateAction, useState} from 'react';

export const LoggedInAthleteContext = createContext<[Athlete | undefined, Dispatch<SetStateAction<Athlete | undefined>>]>(
    [undefined, () => {}]
);

export const LoggedInAthleteProvider: React.FC = ({children}) => {

    const [loggedInAthlete, setLoggedInAthlete] = useState<Athlete>();

    return <LoggedInAthleteContext.Provider value={[loggedInAthlete, setLoggedInAthlete]}>
        {children}
    </LoggedInAthleteContext.Provider>;
};

export interface Athlete {
    athleteId: number;
    firstName: string;
    lastName: string;
    emailAddress?: string;
    accessToken: string;
}
