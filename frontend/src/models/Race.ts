import {DateTime} from 'luxon';

export interface Race {
    raceId: number;
    date: string;
    name: string;
    description?: string;
    distanceInMeters: number;
    earliestTimeRaceCanBeRun: string;
    latestTimeRaceCanBeRun: string;
    timeThatResultsAreReleased: string;
}

export const determineWhetherResultsHaveBeenReleased = (race: Race) => {
    return DateTime.fromISO(race.timeThatResultsAreReleased) <= DateTime.utc();
};

export const determineWhetherRaceIsAcceptingSubmissions = (race: Race) => {
    return DateTime.fromISO(race.earliestTimeRaceCanBeRun) <= DateTime.utc()
        && DateTime.utc() <= DateTime.fromISO(race.timeThatResultsAreReleased);
};

export const determineWhetherRaceIsUpcoming = (race: Race) => {
    return DateTime.utc() < DateTime.fromISO(race.earliestTimeRaceCanBeRun);
};

export const formatRaceDateForDisplay = (race: Race) => DateTime.fromISO(race.date).toFormat('MMMM d');

export const sortByDate = (races: Race[]) => {
    return races.sort((a: Race, b: Race) => DateTime.fromISO(a.date).valueOf() - DateTime.fromISO(b.date).valueOf())
};
