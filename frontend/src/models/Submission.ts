import {Duration} from 'luxon';

export interface Submission {
    raceId: number;
    athleteId: number;
    activityId: number;
    firstName: string;
    lastName: string;
    raceTimeInSeconds: number | undefined;
    sex?: 'Male' | 'Female';
}

export const formatRaceTimeForDisplay = (submission: Submission) => {
    if (!submission.raceTimeInSeconds) {
        return 'Unable to determine time 🤔';
    }
    return Duration.fromObject({seconds: submission.raceTimeInSeconds}).toFormat('mm:ss');
};
