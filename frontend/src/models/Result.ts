import {Submission} from './Submission';

export interface Result extends Submission {
    overallPosition: number;
    malePosition?: number;
    femalePosition?: number;
}

export const convertSubmissionsToResults = (submissions: Submission[]) => {

    let nextMalePosition = 1;
    let nextFemalePosition = 1;
    let nextOverallPosition = 1;

    const results = [];
    for (const submission of sortByTime(submissions)) {
        results.push({
            ...submission,
            overallPosition: nextOverallPosition++,
            malePosition: submission.sex === 'Male' ? nextMalePosition++ : undefined,
            femalePosition: submission.sex === 'Female' ? nextFemalePosition++ : undefined
        });
    }
    return results;
};

const sortByTime = (submissions: Submission[]) => {
    return submissions.sort((a, b) => {
        if (!a.raceTimeInSeconds) {
            return 1;
        }
        if (!b.raceTimeInSeconds) {
            return -1;
        }
        return a.raceTimeInSeconds - b.raceTimeInSeconds;
    })
};

