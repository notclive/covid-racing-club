import {DateTime, Duration} from 'luxon';

export interface PotentialSubmission {
    activityId: number;
    name: string;
    startTime: string;
    eligibility: Eligibility;
    raceTimeInSeconds?: number;
}

type Eligibility = 'Eligible' | 'NotRun' | 'TooShort';

export const formatRaceTimeForDisplay = (submission: PotentialSubmission) => {
    if (!submission.raceTimeInSeconds) {
        return 'Unable to determine time 🤔';
    }
    return Duration.fromObject({seconds: submission.raceTimeInSeconds}).toFormat('mm:ss');
};

export const formatActivityTimeForDisplay = (submission: PotentialSubmission) => {
    return DateTime.fromISO(submission.startTime)
        .toLocaleString({
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
        })
};
