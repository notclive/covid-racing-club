import {DateTime} from 'luxon';

export const isSameDayIgnoringTimeZone = (dayA: DateTime, dayB: DateTime) => {
    return dayA.year === dayB.year && dayA.month === dayB.month && dayA.day === dayB.day;
};

export const formatFutureDateForDisplay = (futureDate: DateTime) => {
    const calendarDaysAway = localMidnightsBetweenNowAndThen(futureDate);
    if (calendarDaysAway === 1) {
        return 'tomorrow'
    }
    if (calendarDaysAway > 1) {
        return `in ${calendarDaysAway} days`;
    }
    return `in ${hoursBetweenNowAndThen(futureDate)} hours`;
};

const localMidnightsBetweenNowAndThen = (futureDate: DateTime) => {
    const startOfToday = DateTime.local().startOf('day');
    const startOfFutureDate = futureDate.toLocal().startOf('day');
    return startOfFutureDate.diff(startOfToday).shiftTo('days').days;
};

const hoursBetweenNowAndThen = (futureDate: DateTime) => {
    return Math.ceil(futureDate.diff(DateTime.local()).shiftTo('hours').hours);
};
