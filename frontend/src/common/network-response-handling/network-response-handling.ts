export const verifyResponseSuccessful = (response: Response) => {
    if (!response.ok) {
        throw new Error(`Network request failed, status code was ${response.status}.`);
    }
    return response;
};

export const handleJsonResponse = (response: Response) => {
    return verifyResponseSuccessful(response).json();
};
