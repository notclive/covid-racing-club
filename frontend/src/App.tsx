import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HomePage from './components/pages/home-page/HomePage';
import RacePage from './components/pages/race-page/RacePage';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import {LoggedInAthleteProvider} from './contexts/LoggedInAthleteContext';
import {RedirectToStravaForLogin} from './components/redirects/RedirectToStravaForLogin';
import {RedirectToPageAfterLogin} from './components/redirects/RedirectToPageAfterLogin';
import RaceCalendarPage from './components/pages/race-calendar-page/RaceCalendarPage';
import {Modals} from './components/modals/Modals';
import {Alerts} from './components/alerts/Alerts';

export const App: React.FC = () => {
    return <LoggedInAthleteProvider>
        <Router>
            <div className='d-flex flex-column min-vh-100'>
                <Navbar/>
                <div className='container my-4 flex-grow-1'>
                    <Alerts/>
                    <Modals/>
                    <Switch>
                        <Route path='/login/out'>
                            <RedirectToStravaForLogin/>
                        </Route>
                        <Route path='/login/in'>
                            <RedirectToPageAfterLogin/>
                        </Route>
                        <Route path='/races/:raceId'>
                            <RacePage/>
                        </Route>
                        <Route path='/race-calendar/:year?/:month?'>
                            <RaceCalendarPage/>
                        </Route>
                        <Route path='/'>
                            <HomePage/>
                        </Route>
                    </Switch>
                </div>
                <Footer/>
            </div>
        </Router>
    </LoggedInAthleteProvider>;
};
