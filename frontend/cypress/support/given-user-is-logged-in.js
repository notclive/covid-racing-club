// Cypress clears local storage between tests but not session storage.
Cypress.Commands.add('givenUserIsLoggedIn', () => {
    // /login/in will check session storage for a path to redirect to, this path is stored by <Private>.
    // Clearing local storage ensures that the user is redirected to the homepage after login.
    clearSessionStorage();
    cy.visit('/login/in?code=dummy-strava-code');
    cy.contains('Fast Fred');
});

// Cypress clears local storage between tests but not session storage.
function clearSessionStorage() {
    cy.window().then((win) => {
        win.sessionStorage.clear()
    });
}
