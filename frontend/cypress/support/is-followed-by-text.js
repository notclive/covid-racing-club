Cypress.Commands.add(
    'isFollowedByText',
    {prevSubject: 'element'},
    (beforeElement, followingText) => {
        cy.contains(followingText).then(followingElement => {
            expect(
                indexInPage(beforeElement) < indexInPage(followingElement),
                `Expected to find '${followingText}' after '${beforeElement.html()}'`
            ).to.be.true;
        });
    }
);

function indexInPage(element) {
    const parent = element.parent();
    if (parent.length === 0) {
        return element.index();
    }
    return indexInPage(parent) + '' + element.index();
}
