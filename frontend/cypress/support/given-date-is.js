Cypress.Commands.add('givenDateIs', (luxonDateTime) => {
    // For some reason cy.tick can't be used to move the clock forward.
    // As a workaround, to let givenDateIs be called multiple times,
    // the real clock is restored then the real clock is mocked again with a new time.
    cy.clock().then(clock => clock.restore());
    return cy.clock(luxonDateTime.toMillis());
});
