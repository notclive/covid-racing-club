import {DateTime} from 'luxon';

context('requesting a race reminder', () => {

    it('given my email address is known, when I request a race reminder, then my email address is pre-populated', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        cy.contains('Remind me about this race').click();

        // then
        cy.contains('How would you like to be reminded?')
            .closest('form').find('input')
            .should('have.value', 'fast-fred@example.com');
    });

    it('given I am on the home page, when I request a race reminder, then I am notified of success', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        cy.contains('Remind me about this race').click();
        cy.contains('How would you like to be reminded?')
            .closest('form').contains('Email me').click();

        // then
        cy.contains('✉ You will be emailed on race day.');
    });

    it('given I am on the calendar, when I request a race reminder, then I am notified of success', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        // Cannot navigate directly to /race-calendar because login state is not persisted.
        cy.contains('All Races').click();
        cy.contains('Remind me').click();
        cy.contains('How would you like to be reminded?')
            .closest('form').contains('Email me').click();

        // then
        cy.contains('✉ You will be emailed on race day.');
    });
});
