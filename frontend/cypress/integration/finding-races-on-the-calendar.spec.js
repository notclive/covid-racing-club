import {DateTime} from "luxon";

context('finding races on the calendar', () => {

    it('when I visit the race calendar, then I see the current month by default', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/race-calendar');

        // then
        cy.contains('Apr 2020');
    });

    it('when I click the "previous" button, then I see last month\'s races', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/race-calendar');
        cy.contains('⬅').click();

        // then
        cy.contains('Mar 2020');
    });

    it('when I click the "next" button, then I see next month\'s races', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/race-calendar');
        cy.contains('➡').click();

        // then
        cy.contains('May 2020');
    });

    it('when I visit the race calendar, then I see races in the current calendar month', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/race-calendar');

        // then
        cy.contains('Calendar Start');
        cy.contains('Open Race');
        cy.contains('Upcoming Race');
        cy.contains('Calendar End');
    });

    it('when I visit the race calendar, then I don\'t see races outside the current calendar month', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/race-calendar');

        // then
        cy.contains('Calendar Start'); // Ensures that races are loaded and visible.
        cy.contains('Before Calendar').should('not.exist');
        cy.contains('After Calendar').should('not.exist');
    });

    it('given an upcoming race is on the calendar, when I click the enter button, then I am taken to the race page', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/race-calendar');

        // then
        cy.contains('Enter').should('have.attr', 'href', '/races/5');
    });

    it('when I navigate directly to a month, then I see that month\'s races', () => {
        // when
        cy.visit('/race-calendar/2020/5');

        // then
        cy.contains('After Calendar');
    });
});
