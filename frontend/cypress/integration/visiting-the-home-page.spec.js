import {DateTime} from 'luxon';

context('visiting the home page', () => {

    it('given there was a race four days ago, when I visit the home page, then I see the race in the recent races section', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Recent Races')
            .isFollowedByText('Last Monday Race')
            .isFollowedByText('Upcoming Races');
    });

    it('given a closed race has results, when I visit the home page, then I see the name and time of the race winner', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Last Monday Race')
            .isFollowedByText('Fast Fred')
            .isFollowedByText('16:42');
    });

    it('given there was a race five days ago, when I visit the home page, then I do not see the race in the recent races section', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Last Sunday Race').should('not.exist');
    });

    it('given there is a race in seven days, when I visit the home page, then I see the race in the upcoming races section', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Upcoming Races').isFollowedByText('Next Thursday Race');
    });

    it('given an upcoming race is open for submissions, when I click the submit button, then I am taken to the race page', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Submit my Strava activity').should('have.attr', 'href', '/races/5');
    });

    it('given an active race has a description, when I visit the home page, then I see the race description', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Open Race').isFollowedByText('Motivational text is not my strong point.');
    });

    it('given an upcoming race has a description, when I visit the home page, then I see the race description', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Next Thursday Race').isFollowedByText('You will not want to miss this race.');
    });

    it('given there is a race in eight days, when I visit the home page, then I do not see the race in the upcoming races section', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/');

        // then
        cy.contains('Next Friday Race').should('not.exist');
    });
});
