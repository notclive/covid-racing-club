import {DateTime} from 'luxon';

context('posting a race submission', () => {

    it('when I visit an open race page, then I see my Strava activities', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        // Cannot navigate directly to /races/5 because login state is not persisted.
        cy.contains('Submit my Strava activity').click();

        // then
        cy.contains('My time trial');
    });

    it('when I submit an eligible Strava activity, then I am notified of success', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        // Cannot navigate directly to /races/5 because login state is not persisted.
        cy.contains('Submit my Strava activity').click();
        cy.contains('Submit this activity').click();

        // then
        cy.contains('Your activity was submitted successfully.');
    });

    it('given I have submitted an eligible Strava activity, when 3 days have passed, then my submission appears on the results page', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();
        // Cannot navigate directly to /races/5 because login state is not persisted.
        cy.contains('Submit my Strava activity').click();
        cy.contains('Submit this activity').click();

        // when
        cy.givenDateIs(DateTime.local(2020, 4, 13, 12));

        // then
        cy.visit('/races/5');
        cy.contains('1');
        cy.contains('Fast Fred');
        cy.contains('18:30');
    });
});
