import {DateTime} from "luxon";

context('viewing race results', () => {

    it('when I view a closed race, then results are ordered fastest first', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/races/1');

        // then
        cy.contains('Fast Fred')
            .isFollowedByText('Quick Kate')
            .isFollowedByText('Moderate Mike')
            .isFollowedByText('Average Alice')
            .isFollowedByText('Slow Sam');

        cy.contains('16:04')
            .isFollowedByText('21:44')
            .isFollowedByText('22:09')
            .isFollowedByText('25:05')
            .isFollowedByText('30:52');
    });

    it('when I view a closed race, then gender results are shown where gender is known', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));

        // when
        cy.visit('/races/1');

        // then
        cy.contains('1')
            .isFollowedByText('Male 1 of 2')
            .isFollowedByText('Fast Fred');

        cy.contains('2')
            .isFollowedByText('Female 1 of 2')
            .isFollowedByText('Quick Kate');

        cy.contains('4')
            .isFollowedByText('Female 2 of 2')
            .isFollowedByText('Average Alice');

        cy.contains('5')
            .isFollowedByText('Male 2 of 2')
            .isFollowedByText('Slow Sam');
    });
});
