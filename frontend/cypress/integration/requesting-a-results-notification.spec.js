import {DateTime} from 'luxon';

context('requesting a results notification', () => {

    it('given my email address is known, when I request a results notification, then my email address is pre-populated', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        cy.contains('Email me when results are released').click();

        // then
        cy.contains('How would you like to be notified?')
            .closest('form').find('input')
            .should('have.value', 'fast-fred@example.com');
    });

    it('given I am on the home page, when I request a results notification, then I am notified of success', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        cy.contains('Email me when results are released').click();
        cy.contains('How would you like to be notified?')
            .closest('form').contains('Email me').click();

        // then
        cy.contains('✉ You will be emailed when results are released.');
    });

    it('given I am on an open race page, when I request a results notification, then I am notified of success', () => {
        // given
        cy.givenDateIs(DateTime.local(2020, 4, 10, 12));
        cy.givenUserIsLoggedIn();

        // when
        // Cannot navigate directly to /races/5 because login state is not persisted.
        cy.contains('Submit my Strava activity').click();
        cy.contains('Email me when results are released').click();
        cy.contains('How would you like to be notified?')
            .closest('form').contains('Email me').click();

        // then
        cy.contains('✉ You will be emailed when results are released.');
    });
});
